var dateOfUpdate = "2019-05-18 00:21:09";
var fullData = [
    {
        "Date": "May 17th, 2019",
        "Title": "FasterClass: Motorcycle Front Tire Wear",
        "Perex": " Dave details 3 common front tire wear patterns involving fork geometry and braking. Remember, your tires are a data collection device, and Dave will help you read that data.",
        "URL": "https://davemosstuning.com/fasterclass-motorcycle-front-tire-wear/",
        "Tags": [
            "braking lines",
            "fork geometry",
            "fork geometry high",
            "fork geometry low",
            "front tire wear"
        ],
        "Categories": [
            "All",
            "classroom",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "May 15th, 2019",
        "Title": "How-To: Motorcycle Fork Valving",
        "Perex": " Dave performs the teardown portion of a fork service which involves removing the cartridge from the fork leg, then the rebound and compression assemblies from the cartridge, then the pistons and shims from the assemblies. Well, the pistons and shims ARE the assemblies, and perform the function commonly called \"valving.\"",
        "URL": "https://davemosstuning.com/how-to-motorcycle-fork-valving/",
        "Tags": null,
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "May 13th, 2019",
        "Title": "RSV4 1100 \u0026 V4S Electronics Comparison",
        "Perex": " RSV4 1100 \u0026 V4S Electronics Comparison   Freemium Video.  Enjoy.  To view Premium content, please subscribe.  Important: Put video description here. Must be one to two sentences....    ",
        "URL": "https://davemosstuning.com/rsv4-1100-v4s-electronics-comparison/",
        "Tags": [
            "Aprilia",
            "Ducati",
            "ecu",
            "electronic suspension",
            "electronics",
            "RSV4",
            "rsv4 1100",
            "v4s"
        ],
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Video"
        ]
    },
    {
        "Date": "May 10th, 2019",
        "Title": "FasterClass: Motorcycle Tire Cold \u0026 Hot Tears",
        "Perex": " The common Cold Tear and the Hot Tear are tire pressure issues. There is also a kind of cold tear from bad geometry. Dave explains what to look for and how to fix the problem. Motorcycle tire wear can ruin a tire very quickly, and they're soooooo expensive, why waste them from.",
        "URL": "https://davemosstuning.com/fasterclass-motorcycle-tire-cold-hot-tears/",
        "Tags": [
            "Bridgestone cold tear",
            "cold tear",
            "dunlop cold tear",
            "geometry cold tear",
            "hot tear",
            "michelin cold tear",
            "pirelli cold tear",
            "rear geometry tear"
        ],
        "Categories": [
            "All",
            "classroom",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "May 8th, 2019",
        "Title": "Fixing Bad Motorcycle Handlebar Ergonomics",
        "Perex": " We're talking full one-piece motorcycle handlebars here, not clip-ons. Full handlebars like you see on a BMX bike. They're ALWAYS off center from the factory, and that's just the first problem.",
        "URL": "https://davemosstuning.com/fixing-bad-motorcycle-handlebar-ergonomics/",
        "Tags": [
            "Aprilia",
            "BMW",
            "ergonomics",
            "ergos",
            "handlebar",
            "s1000r",
            "Tuono"
        ],
        "Categories": [
            "All",
            "How-To",
            "servicing",
            "Video"
        ]
    },
    {
        "Date": "May 6th, 2019",
        "Title": "2 Clicks Out: Street Bike Setups \u0026 Ride Day",
        "Perex": " Dave does some suspension setups at a dealership event, then joins the morning ride to do some fine tuning when the suspension heats up.",
        "URL": "https://davemosstuning.com/2-clicks-out-street-bike-setups-ride-day/",
        "Tags": [
            "2 clicks out",
            "675",
            "ergonomics",
            "R6",
            "sag",
            "settings",
            "TL1000",
            "Yamaha"
        ],
        "Categories": [
            "2 clicks out",
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "May 3rd, 2019",
        "Title": "FasterClass: Performance Motorcycle Tires",
        "Perex": " Remember, there are no bad tires in the 21st Century, just misunderstood or mistreated tires. Dave explains how to get actual performance out of performance motorcycle tires.",
        "URL": "https://davemosstuning.com/fasterclass-performance-motorcycle-tires/",
        "Tags": null,
        "Categories": [
            "All",
            "classroom",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "May 2nd, 2019",
        "Title": "Trash or Treasure: GSX-R750 Rebuild Front End",
        "Perex": " With the forks all freshened up we reassemble the front end of our precious 2005 Suzuki GSX-R750.",
        "URL": "https://davemosstuning.com/trash-or-treasure-gsx-r750-rebuild-front-end/",
        "Tags": null,
        "Categories": [
            "All",
            "How-To",
            "servicing",
            "Video"
        ]
    },
    {
        "Date": "April 29th, 2019",
        "Title": "Comparison Test: V4S vs RSV4 1100 Factory",
        "Perex": " Mr Moss test rides and tunes the Ducati V4S and the Aprilia RSV4 1100 Factory back to back. What's good, what's bad, what's fugly, what's fawesome.",
        "URL": "https://davemosstuning.com/comparison-test-v4s-vs-rsv4-1100-factory/",
        "Tags": null,
        "Categories": [
            "All",
            "Reviews",
            "Suspension",
            "Testing Program",
            "Video"
        ]
    },
    {
        "Date": "April 27th, 2019",
        "Title": "FasterClass: Motorcycle Tire Wear Compression",
        "Perex": " Your motorcycle tire will manifest telltale wear patterns indicating whether your compression settings are too hard, too soft, or just right. Dave explains in detail.",
        "URL": "https://davemosstuning.com/fasterclass-motorcycle-tire-wear-compression/",
        "Tags": null,
        "Categories": [
            "All",
            "classroom",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "April 25th, 2019",
        "Title": "Trash or Treasure: Yamaha YZF 750SP",
        "Perex": " Trash or Treasure: Yamaha YZF 750SP",
        "URL": "https://davemosstuning.com/16038-2/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Video"
        ]
    },
    {
        "Date": "April 22nd, 2019",
        "Title": "2 Clicks Out: Tuono Suspension Setup",
        "Perex": " There aren't too many naked, sit upright superbikes. The Aprilia Tuono is the only one that comes to mind. Luckily it comes with top tier suspension to help manage all that performance.",
        "URL": "https://davemosstuning.com/2-clicks-out-tuono-suspension-setup/",
        "Tags": null,
        "Categories": [
            "2 clicks out",
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 19th, 2019",
        "Title": "FasterClass: Motorcycle Tire Age, Discoloration, Cracking",
        "Perex": " We've bundled some odd but common tire problems into one video. Tires can get too old, they can become discolored, and they can crack...",
        "URL": "https://davemosstuning.com/fasterclass-motorcycle-tire-age-discoloration-cracking/",
        "Tags": null,
        "Categories": [
            "All",
            "How-To",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "April 18th, 2019",
        "Title": "GSX-R750 \u0026 600 Fork Oil Change 2004-05",
        "Perex": " Dave changes the fork oil on the Trash or Treasure GSX-R750 forks in preparation for rebuilding the front end. This fork oil change is the same for the 04-05 GSX-R600.",
        "URL": "https://davemosstuning.com/gsx-r750-600-fork-oil-change-2004-05/",
        "Tags": null,
        "Categories": [
            "All",
            "classroom",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 15th, 2019",
        "Title": "2 Clicks Out: Street Triple 675 Suspension Setup",
        "Perex": " The Triumph Street Triple 675 is an extremely popular bike in Northern California. Based on casual observation the 675 series is their best selling bike, and might even out sell the rest of their models combined. Dave, of course, knows exactly how to tune the flagship of his native Britain.",
        "URL": "https://davemosstuning.com/2-clicks-out-street-triple-675-suspension-setup/",
        "Tags": null,
        "Categories": [
            "2 clicks out",
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 12th, 2019",
        "Title": "FasterClass: Motorcycle Tire Flat Spots",
        "Perex": " Motorcycle tire flat spots, we all get them on our street bike tires. You can actually get them on your front tire as well as the back tire. Dave Moss explains.",
        "URL": "https://davemosstuning.com/fasterclass-motorcycle-tire-flat-spots/",
        "Tags": null,
        "Categories": [
            "All",
            "classroom",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "April 10th, 2019",
        "Title": "Trash or Treasure: GSX-R750 Rear End Rebuild",
        "Perex": " Having discovered most of the trash and most of the treasure hidden in depths of our 2005 Suzuki GSX-R750, we begin the rebuild starting with the arse end; the swingarm (swinging arm in British), shock, and linkage.",
        "URL": "https://davemosstuning.com/trash-or-treasure-gsx-r750-rear-end-rebuild/",
        "Tags": null,
        "Categories": [
            "2 clicks out",
            "All",
            "How-To",
            "servicing",
            "Video"
        ]
    },
    {
        "Date": "April 8th, 2019",
        "Title": "2 Clicks Out: R1200GS vs Super Tenere",
        "Perex": " We don't see many adventure touring bikes, but they benefit greatly from tuned suspension just like any other motorcycle. A BMW R1200GS and Yamaha Super Tenere found their way into Dave's chock for some suspension healing.",
        "URL": "https://davemosstuning.com/2-clicks-out-r1200gs-vs-super-tenere/",
        "Tags": null,
        "Categories": [
            "2 clicks out",
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 5th, 2019",
        "Title": "FasterClass: Motorcycle Street Tire Rebound Wear",
        "Perex": " You've seen Dave read tires and point out what he sees. In this tutorial he focuses exclusively on how to read street tire rebound wear. Proper fork and shock rebound adjustment have the single greatest affect on the handling of your motorcycle.",
        "URL": "https://davemosstuning.com/fasterclass-motorcycle-street-tire-rebound-wear/",
        "Tags": null,
        "Categories": [
            "All",
            "classroom",
            "How-To",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "April 3rd, 2019",
        "Title": "Trash or Treasure: GSX-R750 Front End Tear Down",
        "Perex": "Having torn into the arse end of our 2005 Suzuki GSX-R750, we now investigate the front end. Will we find more treasure than trash up in there?",
        "URL": "https://davemosstuning.com/trash-or-treasure-gsx-r750-front-end-tear-down/",
        "Tags": null,
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2019",
        "Title": "2 Clicks Out: CBR600RR Suspension Setup",
        "Perex": " The Honda CBR600RR has been discontinued, just like the Ford Mustang and Chevy Camaro were for a time. Unlike those cars, every generation CBR600RR is a vastly superior performance machine. With a proper suspension setup any CBR600RR model can crush the newest Mustang or Camaro, even with a passenger perched on the pillion.",
        "URL": "https://davemosstuning.com/2-clicks-out-cbr600rr-suspension-setup/",
        "Tags": null,
        "Categories": [
            "2 clicks out",
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 29th, 2019",
        "Title": "Contact Patch: Reading Motorcycle Street Tires",
        "Perex": " This is the 3rd installment of Dave's street tire reading from 2018. We condensed all the treaded tire reads into 3 videos so you can focus on that skill set.",
        "URL": "https://davemosstuning.com/contact-patch-reading-motorcycle-street-tires/",
        "Tags": null,
        "Categories": [
            "All",
            "How-To",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2019",
        "Title": "Trash or Treasure 750: Rear End Tear Down",
        "Perex": " Trash or Treasure 750: Rear End Tear Down   Freemium Video.  Enjoy.  To view Premium content, please subscribe or login.  Time to dive into our 2005 Suzuki GSX-R750 and determine how much of a gem we actually bought. Is it a diamond in the rough, or just cubic zirconia?    ",
        "URL": "https://davemosstuning.com/trash-or-treasure-750-rear-end-tear-down/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "How-To",
            "servicing",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 25th, 2019",
        "Title": "2 Clicks Out: 899 Panigale Suspension Setup",
        "Perex": " Why should you set sag first before adjusting rebound and compression? Here's the answer plus the baseline suspension issues common to all Ducati 899 Panigales.",
        "URL": "https://davemosstuning.com/2-clicks-out-899-panigale-suspension-setup/",
        "Tags": [
            "Ducati",
            "settings",
            "street",
            "Suspension"
        ],
        "Categories": [
            "2 clicks out",
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 22nd, 2019",
        "Title": "Contact Patch: Reading DOT Tires",
        "Perex": " Our second installment out of three this spring. Another batch of DOT tire reads with Dave. DOT tires have tread patterns which makes for easier tire reading vs slicks.",
        "URL": "https://davemosstuning.com/contact-patch-reading-dot-tires/",
        "Tags": [
            "Contact Patch",
            "how to",
            "tires"
        ],
        "Categories": [
            "All",
            "Tires",
            "Uncategorized",
            "Video"
        ]
    },
    {
        "Date": "March 21st, 2019",
        "Title": "AFM Round 1, Button Willow Raceway March 16th and 17th",
        "Perex": "Getting off the plane from New Zealand and going straight to Thunderhill for the 3 day opener to the Carter's at the Track 2019 season was a good idea :) Apart from all the work, getting Miles set up and loving my \"B\" 450 race bike that he bought from me was an even better idea. [...]",
        "URL": "https://davemosstuning.com/afm-round-1-button-willow-raceway-march-16th-and-17th/",
        "Tags": [
            "AFM",
            "racing",
            "Track"
        ],
        "Categories": [
            "All",
            "Article",
            "Free",
            "Racing"
        ]
    },
    {
        "Date": "March 20th, 2019",
        "Title": "Fork Rebound: Fast Slow or Just Right",
        "Perex": " This is a companion video to our Shock Rebound: Fast Slow or Just Right video. We're getting you eye calibrated in preparation for your best riding season ever.",
        "URL": "https://davemosstuning.com/fork-rebound-fast-slow-or-just-right/",
        "Tags": [
            "2 clicks out",
            "forks",
            "settings",
            "Track"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 18th, 2019",
        "Title": "2 Clicks Out: R1M Suspension For Different Rider Weights",
        "Perex": " Dave tunes the Yamaha R1M stock suspension for a rider weighing 220+ pounds (100+ kilos) and a rider weighing 160 pounds (73 kilos). One setting does not fit all. Here's the first video we shot with Will, the bigger rider. https://davemosstuning.com/2-clicks-out-yamaha-r1m-quick-tune/",
        "URL": "https://davemosstuning.com/2-clicks-out-r1m-suspension-for-different-rider-weights/",
        "Tags": [
            "forks",
            "settings",
            "Shocks",
            "Suspension",
            "Track",
            "Yamaha"
        ],
        "Categories": [
            "2 clicks out",
            "All",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 15th, 2019",
        "Title": "Contact Patch: Slick Tire Reading",
        "Perex": " Reading slick tires is a bit tougher than reading treaded tires. We've compiled all of Dave's recent reading of slick tires so you can wrap your head around the concept.",
        "URL": "https://davemosstuning.com/contact-patch-slick-tire-reading/",
        "Tags": [
            "Contact Patch",
            "settings",
            "tires",
            "Track"
        ],
        "Categories": [
            "All",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 13th, 2019",
        "Title": "Diagnosis: Running Wide On Corner Exit",
        "Perex": " In this situation, as you roll on the throttle your bike will not hold its line exiting the corner, requiring you to close the throttle or increase your lean angle to maintain your direction.",
        "URL": "https://davemosstuning.com/diagnosis-running-wide-on-corner-exit/",
        "Tags": [
            "Fasterclass",
            "Lines",
            "Reference Points",
            "Track"
        ],
        "Categories": [
            "All",
            "classroom",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 11th, 2019",
        "Title": "2 Clicks Out: Ducati Monster Suspension Setup",
        "Perex": " The Ducati Monster is, like most Ducati's, a work of art. Dave Moss reveals Italian Super Models aren't just eye candy, they can cook too.",
        "URL": "https://davemosstuning.com/2-clicks-out-ducati-monster-suspension-setup/",
        "Tags": [
            "Ducati",
            "settings",
            "street",
            "Suspension"
        ],
        "Categories": [
            "2 clicks out",
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 10th, 2019",
        "Title": "Mosscast: Adding Luggage To Your Bike",
        "Perex": " Dave details the handling and suspension issues associated with adding luggage to your motorcycle using the Yamaha MT-10 as an example.",
        "URL": "https://davemosstuning.com/mosscast-adding-luggage-to-your-bike/",
        "Tags": [
            "Luggage \u0026 Pillion",
            "Podcast",
            "settings",
            "Suspension"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 8th, 2019",
        "Title": "Contact Patch: DOT Tire Reads from 2018",
        "Perex": " In preparation for the riding season we've compiled all the various tire reading Dave Moss did in 2018 into 3 videos. This is Part 1 of 3. You will be reading tires with confidence in 2019 after you watch these.",
        "URL": "https://davemosstuning.com/contact-patch-dot-tire-reads-from-2018/",
        "Tags": [
            "Contact Patch",
            "tires",
            "wear patterns"
        ],
        "Categories": [
            "All",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 6th, 2019",
        "Title": "Your Suspension Setup Must Match Your Pace",
        "Perex": " The suspension settings appropriate for street riding are inadequate for an average trackday, which settings are in turn inadequate at race pace.",
        "URL": "https://davemosstuning.com/your-suspension-setup-must-match-your-pace/",
        "Tags": [
            "settings",
            "street",
            "Suspension",
            "Track"
        ],
        "Categories": [
            "All",
            "classroom",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 4th, 2019",
        "Title": "2 Clicks Out: Yamaha R1 Baseline Suspension Setup",
        "Perex": " While there is no magic suspension setting good for every rider, some bikes do need a certain things done to them immediately for almost everyone. The current generation Yamaha R1 is one of those bikes.",
        "URL": "https://davemosstuning.com/2-clicks-out-yamaha-r1-baseline-suspension-setup/",
        "Tags": [
            "forks",
            "settings",
            "Shocks",
            "Suspension",
            "Track",
            "Yamaha"
        ],
        "Categories": [
            "2 clicks out",
            "All",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 1st, 2019",
        "Title": "Shock Rebound: Fast, Slow, or Just Right",
        "Perex": " You've seen Dave push on bikes to gage shock rebound and then set it correctly. See if you can tell whether shock rebound is fast, slow, or set just right on the bikes in this video.",
        "URL": "https://davemosstuning.com/shock-rebound-fast-slow-or-just-right/",
        "Tags": [
            "Fasterclass",
            "settings",
            "Shocks"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 1st, 2019",
        "Title": "Shock Rebound: Fast, Slow, or Just Right (Freemium)",
        "Perex": " You've seen Dave push on bikes to gage shock rebound and then set it correctly. See if you can tell whether shock rebound is fast, slow, or set just right on the bikes in this video.",
        "URL": "https://davemosstuning.com/shock-rebound-fast-slow-or-just-right-freemium/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "February 27th, 2019",
        "Title": "Diagnosis: Bike Falls Into The Turn",
        "Perex": " As you begin to turn your motorcycle it begins to lean over. Sometimes that rate of lean gets faster the more you turn, like the bike wants to fall over. Maybe you haven't experienced that yet, but someday you will.",
        "URL": "https://davemosstuning.com/diagnosis-bike-falls-into-the-turn/",
        "Tags": [
            "Fasterclass",
            "Reference Points",
            "settings",
            "Track"
        ],
        "Categories": [
            "All",
            "classroom",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "February 25th, 2019",
        "Title": "2 Clicks Out: Kawasaki Concours14 Suspension Settings",
        "Perex": " The Kawasaki Concours14 has a great deal of suspension adjustability. Dave provides solo and passenger settings for the rider.",
        "URL": "https://davemosstuning.com/2-clicks-out-kawasaki-concours14-suspension-settings/",
        "Tags": [
            "forks",
            "Kawasaki",
            "settings",
            "Shocks",
            "street",
            "Suspension"
        ],
        "Categories": [
            "2 clicks out",
            "All",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "February 24th, 2019",
        "Title": "Mosscast: Real World Changes \u0026 Testing MT-10",
        "Perex": " Dave Moss continues the process of refining motorcycle geometry and suspension setup using an Yamaha MT-10 as an example.",
        "URL": "https://davemosstuning.com/mosscast-real-world-changes-testing-mt-10/",
        "Tags": [
            "forks",
            "settings",
            "Shocks",
            "street",
            "Suspension",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "February 22nd, 2019",
        "Title": "FasterClass: Dirt Bike Suspension Basics",
        "Perex": " Like many of us, Dave Moss rides dirt bikes as well as street bikes. He's been tuning dirt bike suspension almost as long as he's been tuning road bikes. Here's are the basics of dirt bike suspension, tire pressures, ergonomics, etc.",
        "URL": "https://davemosstuning.com/fasterclass-dirt-bike-suspension-basics/",
        "Tags": [
            "dirt bike",
            "settings",
            "Suspension"
        ],
        "Categories": [
            "All",
            "classroom",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "February 20th, 2019",
        "Title": "GSX-R1000 Fork Seal \u0026 Oil Change 2003-08",
        "Perex": " How to tear down the forks of the 2003-2008 Suzuki GSX-R1000, drain the oil, replace the seal, and refill to the proper oil level.",
        "URL": "https://davemosstuning.com/gsx-r1000-fork-seal-oil-change-2003-08/",
        "Tags": [
            "forks",
            "how to",
            "servicing",
            "Suzuki"
        ],
        "Categories": [
            "All",
            "classroom",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "February 18th, 2019",
        "Title": "2 Clicks Out: Cro-Magnon Motorcycle?",
        "Perex": " Motorcycle years are akin to dog years and computer years (mobile phone years are like fruit fly weeks). Are motorcycles built around the turn of the century Cro-Motorbike or Moto Sapiens? You decide.",
        "URL": "https://davemosstuning.com/2-clicks-out-cro-magnon-motorcycle/",
        "Tags": [
            "Ability",
            "forks",
            "settings",
            "Shocks",
            "Suspension",
            "Track"
        ],
        "Categories": [
            "2 clicks out",
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "February 17th, 2019",
        "Title": "Mosscast: Stock Suspension Changes MT-10",
        "Perex": " In this podcast Dave details the changes to be made to stock suspension using a Yamaha MT-10 as an example.",
        "URL": "https://davemosstuning.com/mosscast-stock-suspension-changes-mt-10/",
        "Tags": [
            "Podcast",
            "settings",
            "street",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "February 15th, 2019",
        "Title": "Diagnosis: Wallowing On Corner Exit",
        "Perex": " When your motorcycle is wallowing out of turn it is a singular experience. To describe it in words is difficult, like trying to describe the taste of salt. You know wallowing when you taste it.  Thumbnail pic by Bob Jones.",
        "URL": "https://davemosstuning.com/diagnosis-wallowing-on-corner-exit/",
        "Tags": [
            "Fasterclass",
            "settings",
            "Suspension",
            "Track"
        ],
        "Categories": [
            "All",
            "classroom",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "February 13th, 2019",
        "Title": "GSX-R1000 Fork Oil Change 03-08",
        "Perex": " Dave goes through the process, step by step, of changing fork oil on the 2003 through 2008 Suzuki GSX-R1000.",
        "URL": "https://davemosstuning.com/gsx-r1000-fork-oil-change-03-08/",
        "Tags": [
            "forks",
            "servicing",
            "Suzuki"
        ],
        "Categories": [
            "All",
            "classroom",
            "Free",
            "How-To",
            "servicing",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "February 10th, 2019",
        "Title": "Mosscast: Adjustments For The Street",
        "Perex": " Mr. Moss walks us through the basic suspension adjustments one should make to their motorcycle for street riding.",
        "URL": "https://davemosstuning.com/mosscast-adjustments-for-the-street/",
        "Tags": [
            "Podcast",
            "settings",
            "street"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "February 8th, 2019",
        "Title": "Contact Patch: Slick Tire Hot Pressures",
        "Perex": " Dave gives the hot pressures for Bridgestone, Dunlop, Pirelli, and Michelin slick tires. All good things come from God, and grip is really really good.",
        "URL": "https://davemosstuning.com/contact-patch-slick-tire-hot-pressures/",
        "Tags": [
            "Fasterclass",
            "settings",
            "tires",
            "Track"
        ],
        "Categories": [
            "All",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "February 6th, 2019",
        "Title": "Diagnosis: Mid Corner Instability",
        "Perex": " This is when your motorcycle wobbles, jumps, or moves around erratically as you approach the apex of a corner; when your at max lean transitioning from brakes to throttle. Why is that happening, and how do you fix it? Watch and Learn.",
        "URL": "https://davemosstuning.com/diagnosis-mid-corner-instability/",
        "Tags": [
            "Fasterclass",
            "how to",
            "Track"
        ],
        "Categories": [
            "All",
            "classroom",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "February 5th, 2019",
        "Title": "2 Clicks Out: Triumph 675 Suspension Setup",
        "Perex": " Dave Moss raced the Triumph 675 for years and knows it like every square inch of his glorious birthday suit. There's a great treatise on rebound tire wear, another on street vs track and commuting vs canyons, etc etc.",
        "URL": "https://davemosstuning.com/2-clicks-out-triumph-675-suspension-setup/",
        "Tags": [
            "sportbike",
            "street",
            "Suspension",
            "Triumph"
        ],
        "Categories": [
            "2 clicks out",
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "February 3rd, 2019",
        "Title": "Mosscast: Your Bike Your Suspension",
        "Perex": " Suspension exists to serve the rider. Much, if not all of it is adjustable... for a reason. 99 times out of 100, suspension needs to be adjusted. That's why it's adjustable; so it can be of better service to you, the rider.",
        "URL": "https://davemosstuning.com/mosscast-your-bike-your-suspension/",
        "Tags": [
            "Podcast",
            "street",
            "Suspension",
            "Track"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "February 1st, 2019",
        "Title": "Dave Moss Personal Suspension Setup",
        "Perex": " You see Dave do motorcycle suspension setups for other riders all the time. What about his bike? What is Dave Moss' own gofast setup?",
        "URL": "https://davemosstuning.com/dave-moss-personal-suspension-setup/",
        "Tags": [
            "Geometry",
            "racing",
            "settings"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "February 1st, 2019",
        "Title": "2018 Yamaha Tracer GT first ride at pace, fully loaded with luggage",
        "Perex": " Premium Article Excerpt. To read the full article please Subscribe or Login.",
        "URL": "https://davemosstuning.com/2018-yamaha-tracer-gt-first-ride-at-pace-fully-loaded-with-luggage/",
        "Tags": [
            "settings",
            "standard",
            "street",
            "Suspension",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Article",
            "How-To",
            "Testing Program"
        ]
    },
    {
        "Date": "January 30th, 2019",
        "Title": "Diagnosis: Constant Handle Bar Pressure",
        "Perex": " The symptoms of this ailment are your constant need to push on your motorcycle's handle bar to keep in in the turn, and/or your bike is difficult to turn at all; you must really crank on your handle bars to make it turn. Here's the cause and the fix.",
        "URL": "https://davemosstuning.com/diagnosis-constant-handle-bar-pressure/",
        "Tags": [
            "ergonomics",
            "settings",
            "street"
        ],
        "Categories": [
            "All",
            "classroom",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "January 30th, 2019",
        "Title": "2018 Yamaha Tracer 900 GT, New Zealand",
        "Perex": " Premium Article Excerpt. To read the full article please Subscribe or Login.",
        "URL": "https://davemosstuning.com/2018-yamaha-tracer-900-gt-new-zealand/",
        "Tags": [
            "ergonomics",
            "forks",
            "Geometry",
            "Shocks",
            "street",
            "Suspension",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Article",
            "How-To",
            "Testing Program"
        ]
    },
    {
        "Date": "January 28th, 2019",
        "Title": "2 Clicks Out: Aftermarket Shock Links",
        "Perex": " We've talked about progressive springs. Most bikes come with progressive shock linkage as well. We've talked about lowering links. You can also get lifting links.",
        "URL": "https://davemosstuning.com/2-clicks-out-aftermarket-shock-links/",
        "Tags": [
            "2 clicks out",
            "settings",
            "Shocks",
            "street"
        ],
        "Categories": [
            "2 clicks out",
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "January 27th, 2019",
        "Title": "Mosscast: Basic Motorcycle Ergonomics",
        "Perex": " For a century the only thing you could adjust ergonomically in a car was the seat forwards and backwards. Then came adjustable seat backs, then tilt steering wheels, etc etc. While motorcycle seats remain nonadjustable, EVERYTHING else is, hello.",
        "URL": "https://davemosstuning.com/mosscast-basic-motorcycle-ergonomics/",
        "Tags": [
            "ergonomics",
            "Podcast",
            "settings",
            "street"
        ],
        "Categories": [
            "All",
            "Video"
        ]
    },
    {
        "Date": "January 25th, 2019",
        "Title": "Suspension Myth 4: The Golden Settings",
        "Perex": " Chasing unicorns. That quest for the magic motorcycle suspension setup, those golden settings that somehow work for that particular motorcycle regardless who rides it; sometimes called \"the final setup.\" Unfortunately there is no suspension pixie dust. Here's why, and what to do about it.",
        "URL": "https://davemosstuning.com/suspension-myth-4-the-golden-settings/",
        "Tags": [
            "street",
            "Suspension",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "January 23rd, 2019",
        "Title": "Diagnosis: Braking Chatter",
        "Perex": " As you fly into a corner and grab the brakes, the front end of your bike begins to vibrate up and down in your hands in a mini hopping stutter. If you watch racing you've seen racers make the motion with their hands to their crew chiefs. Why is your bike doing that and how [...]",
        "URL": "https://davemosstuning.com/diagnosis-braking-chatter/",
        "Tags": [
            "Braking",
            "how to",
            "settings",
            "street",
            "Track"
        ],
        "Categories": [
            "All",
            "classroom",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "January 21st, 2019",
        "Title": "2 Clicks Out: S1000RR DDC Suspension Setup",
        "Perex": " The BMW S1000RR seriously upped the ante when it debuted, and BMW was quick to offer an electronic suspension (DDC) version. Dave tunes a number of those electronic DDC suspension models here.",
        "URL": "https://davemosstuning.com/2-clicks-out-s1000rr-ddc-suspension-setup/",
        "Tags": [
            "BMW",
            "settings",
            "sportbike",
            "Suspension"
        ],
        "Categories": [
            "2 clicks out",
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "January 20th, 2019",
        "Title": "Mosscast: Buying Your Next Bike Criteria MT-10",
        "Perex": " Most of us buy everything on emotion coupled with some degree of impulse. Buying a new motorcycle is no exception. Moss, of course, takes a more rational approach to the motorcycle purchase process.",
        "URL": "https://davemosstuning.com/mosscast-buying-your-next-bike-criteria/",
        "Tags": [
            "ergonomics",
            "Podcast",
            "sportbike",
            "standard"
        ],
        "Categories": [
            "All",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "January 18th, 2019",
        "Title": "GSX-R750 Superbike Super Parts",
        "Perex": " It's been said that when you buy a Porsche, make sure you have a fat maintenance wallet to go with it.  Super bling superbike's require a similar wallet, even ones like this 2004 Suzuki GSX-R750.",
        "URL": "https://davemosstuning.com/gsx-r750-superbike-super-parts/",
        "Tags": [
            "GSXR",
            "servicing",
            "sportbike",
            "Suzuki"
        ],
        "Categories": [
            "All",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "January 16th, 2019",
        "Title": "Navigating a 90° Off Camber Corner",
        "Perex": " Almost every track in the world has an off-camber corner, usually low gear and around 90-ish degrees, give or take. Dave explains the braking and throttle techniques best suited to this type of bend in the road.",
        "URL": "https://davemosstuning.com/navigating-a-90-off-camber-corner/",
        "Tags": [
            "Acceleration",
            "Braking",
            "sportbike",
            "Track"
        ],
        "Categories": [
            "All",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "January 14th, 2019",
        "Title": "2 Clicks Out: 959 Panigale Suspension Settings",
        "Perex": " While we haven't seen sales figures, the Panigale 959 is arguably Ducati's best selling sportbike. We see them ALL the time. Maybe it's the price point. Or maybe it's just a Northern California thing, like the Toyota Prius.",
        "URL": "https://davemosstuning.com/2-clicks-out-panigale-959-suspension-settings/",
        "Tags": [
            "2 clicks out",
            "Ducati",
            "Panigale",
            "Suspension"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "January 13th, 2019",
        "Title": "Mosscast: Tire Tech episode 5",
        "Perex": " In this episode Dave addresses motorcycle tire wear and it's underlying causes of pressure, suspension, and geometry.",
        "URL": "https://davemosstuning.com/mosscast-tire-tech-episode-5/",
        "Tags": [
            "Podcast",
            "tires"
        ],
        "Categories": [
            "All",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "January 11th, 2019",
        "Title": "Fasterclass: Rider Sag",
        "Perex": " Motorcycle Rider Sag is the amount the bike sags under the weight of the rider. This is added to Static Sag to arrive at Total Sag. Sag is a just a place to begin. It will be ridiculously better than what the factory put in your bike, so your bike will ride much better, but [...]",
        "URL": "https://davemosstuning.com/fasterclass-rider-sag/",
        "Tags": [
            "Fasterclass",
            "how to",
            "sag"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "January 10th, 2019",
        "Title": "DMT’s 5 Golden rules for you and your motorcycle by Vikrant Singh",
        "Perex": " Freemium Article.  Enjoy.  To view Premium content, please Subscribe or Login.  5 Golden Rules For Motorcyclists As Explained by Dave Moss   Vikrant Singh Introduction Dave Moss is a man on a mission. He firmly believes that setting up a motorcycle correctly can save lives. And so his mission is to be able to [...]",
        "URL": "https://davemosstuning.com/dmts-5-golden-rules-for-you-and-your-motorcycle-by-vikrant-singh/",
        "Tags": [
            "article",
            "ergonomics",
            "maintenance",
            "Suspension"
        ],
        "Categories": [
            "All",
            "Article",
            "Free",
            "How-To"
        ]
    },
    {
        "Date": "January 9th, 2019",
        "Title": "2 Clicks Out: ZX-10R \u0026 RR Suspension Setup",
        "Perex": " The Kawasaki ZX-10R and ZX-10RR have crushed World Superbike for 7 years now. Why they're not the best selling liter bikes on the planet is a mystery. If you were smart enough to buy one, you'll find some great setup info here.",
        "URL": "https://davemosstuning.com/2-clicks-out-zx-10r-rr-suspension-setup/",
        "Tags": [
            "2 clicks out",
            "how to",
            "Kawasaki",
            "Suspension",
            "ZX-10"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "January 9th, 2019",
        "Title": "Mosscast: Tire Tech episode 4",
        "Perex": " What does proper and improper tire pressure do to motorcycle geometry; to rake and trail for example? Will the motorcycle steer better or worse?",
        "URL": "https://davemosstuning.com/mosscast-tire-tech-episode-4/",
        "Tags": [
            "Podcast",
            "tires"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "January 7th, 2019",
        "Title": "2 Clicks Out: Hayabusa Suspension Setup",
        "Perex": " Dave does a suspension setup on a Suzuki GSX-1300R Hayabusa. This is not a drag race setup. It's a setup for average street and canyon riding. We'll cover drag race setups in another video. Below is the link to Dave Moss and Dave Williams, editor, drag racing. https://davemosstuning.com/dave-daver-go-drag-racing/",
        "URL": "https://davemosstuning.com/2-clicks-out-hayabusa-suspension-setup/",
        "Tags": [
            "2 clicks out",
            "Hyabusa",
            "Suspension",
            "Suzuki"
        ],
        "Categories": [
            "2 clicks out",
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "January 7th, 2019",
        "Title": "Dave \u0026 Daver Go Drag Racing",
        "Perex": " Once upon a time Dave Moss and Dave Williams had tons and tons of fun.  Here's an example.",
        "URL": "https://davemosstuning.com/dave-daver-go-drag-racing/",
        "Tags": [
            "racing",
            "Video"
        ],
        "Categories": [
            "All",
            "Free",
            "Racing",
            "Video"
        ]
    },
    {
        "Date": "January 5th, 2019",
        "Title": "Fasterclass: Suspension Revalve \u0026 Respring",
        "Perex": " How do you know when your weight and/or riding ability has exceeded your motorcycle's suspension components? How do you know when it's time to upgrade your suspension, to revalve and/or respring? Dave Moss reviews the telltale signs, and provides some guidelines.",
        "URL": "https://davemosstuning.com/fasterclass-suspension-revalve-respring/",
        "Tags": [
            "classroom",
            "respring",
            "revalve"
        ],
        "Categories": [
            "All",
            "classroom",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "January 2nd, 2019",
        "Title": "2 Clicks Out: Multistrada 1200 Suspension Settings",
        "Perex": " The Ducati Multistrada 1200's in this video are all manual suspension bikes. 2 of them are brand new off the showroom floor, so you'll get good setup info there. The 3rd bike is used with 10,000 miles (16,000 km), and the owner is dissatisfied with the suspension setup he received from another tuner, so he's [...]",
        "URL": "https://davemosstuning.com/2-clicks-out-multistrada-1200-suspension-settings/",
        "Tags": [
            "2 clicks out",
            "Ducati",
            "Multistrada",
            "Suspension"
        ],
        "Categories": [
            "2 clicks out",
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "December 31st, 2018",
        "Title": "2 Clicks Out: R6 Suspension Setup",
        "Perex": " In this video we're looking at the new generation, the 2017 \u0026 2018 Yamaha R6 here. Dave tunes a couple of stock bikes, so there's good stock suspension setup info there, and then a couple with upgraded suspension. As usual, once you approach Trackday fast group and club racing speeds, the stock bits just can't [...]",
        "URL": "https://davemosstuning.com/2-clicks-out-r6-suspension-setup/",
        "Tags": [
            "2 clicks out",
            "how to",
            "R6",
            "Suspension",
            "Yamaha"
        ],
        "Categories": [
            "2 clicks out",
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "December 30th, 2018",
        "Title": "Mosscast: Tire Tech episode 3",
        "Perex": " Is one better for you than another? A motorcyclist's riding experience differs depending on the brand of tire they choose. Suspension settings for the same bike and rider differ based on tire brand as well. Why?",
        "URL": "https://davemosstuning.com/mosscast-tire-tech-episode-3/",
        "Tags": [
            "Podcast",
            "tires"
        ],
        "Categories": [
            "All",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "December 29th, 2018",
        "Title": "Moss Unsprung: Fork Oil",
        "Perex": " Why don't you change your fork oil as often as you change your engine oil? Which fork oil should you use? Moss answers these questions and many more in this recording of a live stream show from the OnTheThrottle days. Its vintage video quality, but the info is priceless. This was top flight live streaming [...]",
        "URL": "https://davemosstuning.com/moss-unsprung-fork-oil/",
        "Tags": [
            "forks",
            "how to",
            "servicing"
        ],
        "Categories": [
            "All",
            "How-To",
            "servicing",
            "Video"
        ]
    },
    {
        "Date": "December 28th, 2018",
        "Title": "Fasterclass: Setting Fork Static Sag",
        "Perex": " Static sag is the amount the suspension compressions under the motorcycle's weight WITHOUT the rider. You achieve proper fork static sag in a slightly different way than you achieve shock static sag. You always see Dave measure shock static sag, but almost never fork static sag, but fear not, he's accounting for it... just never [...]",
        "URL": "https://davemosstuning.com/fasterclass-setting-fork-static-sag/",
        "Tags": [
            "forks",
            "how to",
            "Suspension"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "December 25th, 2018",
        "Title": "2 Clicks Out: Hypermotard Suspension Setup",
        "Perex": " A couple of Ducati Hypermotards roll into Dave's suspension tuning chock.     Freemium Video.  Enjoy.  To view Premium content, please Subscribe or Login.     Want to see the full video?    Get Dave Moss Total Access    Already a Member?     ",
        "URL": "https://davemosstuning.com/2-clicks-out-hypermotard-suspension-setup/",
        "Tags": [
            "Ducati",
            "Hypermotard",
            "Suspension"
        ],
        "Categories": [
            "2 clicks out",
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "December 24th, 2018",
        "Title": "Contact Patch: All Hypersport Tire Pressures",
        "Perex": " Dave gives the starting cold pressures for every hypersport tire on the market, for both street and track riding.",
        "URL": "https://davemosstuning.com/contact-patch-all-hypersport-tire-pressures/",
        "Tags": [
            "how to",
            "Hypersport",
            "tires"
        ],
        "Categories": [
            "All",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "December 21st, 2018",
        "Title": "2 Clicks Out: RSV4 Suspension Settings",
        "Perex": " The Aprilia RSV4 is built for fun at one speed and one speed only, WOT or Wide Open Throttle. If the suspension setup is wrong, it's no fun at all. Luckily, Dave knows fun.",
        "URL": "https://davemosstuning.com/2-clicks-out-rsv4-suspension-settings/",
        "Tags": [
            "Aprilia",
            "RSV4",
            "Suspension"
        ],
        "Categories": [
            "2 clicks out",
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "December 19th, 2018",
        "Title": "Standard Dirt Bike Fork Seal \u0026 Service",
        "Perex": " Dave performs a full fork service and seal change on a standard set of dirt bike forks (not dual chamber).",
        "URL": "https://davemosstuning.com/dirt-bike-fork-seal-service/",
        "Tags": [
            "dirt bike",
            "forks",
            "servicing"
        ],
        "Categories": [
            "All",
            "How-To",
            "servicing",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "December 19th, 2018",
        "Title": "Contact Patch: Dunlop Q3+ on MT-10",
        "Perex": " A rider brings his FZ-10 / MT-10 to the track for the first time shod with fresh Dunlop Q3+ hypersport tires. We start with cold pressures, then monitor and adjust them as the day progresses.",
        "URL": "https://davemosstuning.com/contact-patch-dunlop-q3-on-mt-10/",
        "Tags": [
            "Dunlop",
            "how to",
            "Hypersport",
            "tires"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "December 18th, 2018",
        "Title": "Mosscast: Tire Tech episode 2",
        "Perex": " What do you inflate your tires with? The closer you get to the sharp end, the more it matters.",
        "URL": "https://davemosstuning.com/mosscast-tire-tech-episode-2/",
        "Tags": [
            "Podcast",
            "tires"
        ],
        "Categories": [
            "All",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "December 17th, 2018",
        "Title": "2 Clicks Out: A Dojo of ZX-6R’s",
        "Perex": " The Kawasaki Ninja ZX-6R won a grundle of World Supersport titles recently. Of course the showroom version comes with an extra 36 cc's of go-fast.",
        "URL": "https://davemosstuning.com/2-clicks-out-a-dojo-of-zx-6rs/",
        "Tags": [
            "Kawasaki",
            "Suspension",
            "ZX6R"
        ],
        "Categories": [
            "2 clicks out",
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "December 14th, 2018",
        "Title": "2009 CRF450X first visit to an MX track and dial in process",
        "Perex": " Premium Article Excerpt. To read the full article please Subscribe or Login.",
        "URL": "https://davemosstuning.com/2009-crf450x-first-visit-to-an-mx-track-and-dial-in-process/",
        "Tags": [
            "CRF450X",
            "dirt",
            "MX"
        ],
        "Categories": [
            "All",
            "Article",
            "How-To",
            "Suspension",
            "Testing Program"
        ]
    },
    {
        "Date": "December 14th, 2018",
        "Title": "Test \u0026 Tune: Honda CRF450X",
        "Perex": " Dave Moss goes dirt biking... in the \"duhrt\". We'll be doing lots of dirt bike content now in addition to all our road bike stuff. Here's the first installment.",
        "URL": "https://davemosstuning.com/test-tune-honda-crf450x/",
        "Tags": [
            "2 clicks out",
            "CRF450X",
            "dirt"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Testing Program",
            "Video"
        ]
    },
    {
        "Date": "December 13th, 2018",
        "Title": "Mosscast: Tire Tech episode 1",
        "Perex": " In this podcast Dave explains the relationship between tire pressure and temperature, and why there is no one magic tire pressure number.",
        "URL": "https://davemosstuning.com/mosscast-tire-tech-episode-1/",
        "Tags": [
            "Podcast",
            "tires"
        ],
        "Categories": [
            "All",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "December 12th, 2018",
        "Title": "2 Clicks Out: Frankenbikes",
        "Perex": " Dave tunes 3 Frankenstein bikes, mid horsepower bikes with upgraded chassis and suspension bits from superbikes. Horsepower, in any amount, is only as good as the chassis' ability to put it to the ground.",
        "URL": "https://davemosstuning.com/2-clicks-out-frankenbikes/",
        "Tags": [
            "2 clicks out",
            "Suspension",
            "Track"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "December 11th, 2018",
        "Title": "Fork Service Big Piston Fork",
        "Perex": " Mr. Moss performs a full service on the infamous Big Piston Fork, a popular fork design on many makes and models over the last few years.",
        "URL": "https://davemosstuning.com/fork-service-big-piston-fork/",
        "Tags": [
            "big piston",
            "forks",
            "Kawasaki",
            "servicing"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "December 11th, 2018",
        "Title": "Trash or Treasure: GSX-R750",
        "Perex": " Mr. Williams, editor, acquired a 2005 Suzuki GSX-R750 for $400. Hasn't run for 7 years, minimum. Will it run? Is it trash or treasure?",
        "URL": "https://davemosstuning.com/trash-or-treasure-gsx-r750/",
        "Tags": [
            "GSXR",
            "servicing",
            "Suzuki"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "December 5th, 2018",
        "Title": "Moss Unsprung: Setting Sag",
        "Perex": " Do national and world level racers set sag? When measuring sag, does it matter if the bike is in a chock, on stands, on the ground with rider on tippy toes? Moss explains in this recording of a live stream show from the OnTheThrottle days. Its vintage video quality, but the info is priceless. This [...]",
        "URL": "https://davemosstuning.com/moss-unsprung-setting-sag/",
        "Tags": [
            "sag",
            "Suspension",
            "Ustream"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "December 3rd, 2018",
        "Title": "2 Clicks Out: Triumph 765 RS Unicorn",
        "Perex": " The Triumph 765 RS is a rare bird, at least in our neck of the woods. Ducs are everywhere, but not Triumphs. Dave finally got to tune one at a recent dealership event, Pro Italia, in Glendale California (a SoCal Duc pond).",
        "URL": "https://davemosstuning.com/2-clicks-out-triumph-765-rs-unicorn/",
        "Tags": [
            "how to",
            "Suspension",
            "Triumph"
        ],
        "Categories": [
            "All",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "November 30th, 2018",
        "Title": "2 Clicks Out: A Flock of Gixxer 750’s",
        "Perex": " The Suzuki GSX-R750 is the last sportbike in that sweet spot between the 600 and the 1000. With a user friendly chassis and great suspension, it's easy to ride fast.",
        "URL": "https://davemosstuning.com/2-clicks-out-a-flock-of-gixxer-750s/",
        "Tags": [
            "2 clicks out",
            "GSXR",
            "how to",
            "Track"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "November 28th, 2018",
        "Title": "Contact Patch: Dunlop Q4 Experiences",
        "Perex": " A gentleman switches from the Dunlop Q3 to the Dunlop Q4 and finds some handling issues in the upgrade. Then a guy argues with Dave about whether is Q4 is bald or not.",
        "URL": "https://davemosstuning.com/contact-patch-dunlop-q4-experiences/",
        "Tags": [
            "2 clicks out",
            "Dunlop",
            "tires",
            "Track"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "November 28th, 2018",
        "Title": "Capit tire warmers",
        "Perex": "Capit Suprema Spina Tire Warmers Home site: https://www.capit.it/en/ USA site: https://www.tawperformance.com/ Tire warmers are only as good as their power supply. Easy....... take a second to think that through....... That is by far the first and most important facet of this type of product. If you have 5 devices running off one thin extension cord, the [...]",
        "URL": "https://davemosstuning.com/capit-tire-warmers/",
        "Tags": [
            "how to",
            "temperatures",
            "tire warmers",
            "Track"
        ],
        "Categories": [
            "All",
            "Article",
            "Free",
            "Reviews",
            "Testing Program"
        ]
    },
    {
        "Date": "November 26th, 2018",
        "Title": "How To: Capit Tire Warmers",
        "Perex": " T.A.W. Performance sent us a set of Capit tire warmers, their Suprema Spina model, to work with. We begin by using them as the foundation for this Tire Warmers 101 video.",
        "URL": "https://davemosstuning.com/how-to-capit-tire-warmers/",
        "Tags": [
            "how to",
            "temperatures",
            "tire warmers",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Reviews",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "November 23rd, 2018",
        "Title": "Moss Unsprung: Bump Absorption",
        "Perex": " How does your motorcycle absorb bumps? It does so via the compression damping circuit in the forks and shock. Moss explains how to set it up properly in this recording of a live stream show from the OnTheThrottle days. Its vintage video quality, but the info is priceless. This was top flight live streaming tech [...]",
        "URL": "https://davemosstuning.com/moss-unsprung-bump-absorption/",
        "Tags": [
            "street",
            "Suspension",
            "Track",
            "Ustream"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "November 21st, 2018",
        "Title": "2 Clicks Out: Mr. \u0026 Mrs. Gixxer 1000",
        "Perex": " A husband and wife come to Dave for therapy, suspension therapy, for their matching Suzuki GSX-R1000's. A 2017 and 2018.",
        "URL": "https://davemosstuning.com/2-clicks-out-mr-mrs-gixxer-1000/",
        "Tags": [
            "2 clicks out",
            "GSXR",
            "street",
            "Suzuki"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "November 19th, 2018",
        "Title": "Test \u0026 Tune: Panigale V4",
        "Perex": " Dave does his second test ride on the Ducati Panigale V4 base model, and tunes the Panigale V4S and V4S Speciale.",
        "URL": "https://davemosstuning.com/test-tune-panigale-v4/",
        "Tags": [
            "2 clicks out",
            "Ducati",
            "how to",
            "Panigale",
            "Suspension"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "November 17th, 2018",
        "Title": "2 Clicks Out: Tuono Fest 2",
        "Perex": " Recently a herd of Aprilia Tuono's stopped for respite at Dave's sport chock. We compiled them for your enjoyment. Even if you don't ride a Tuono, there are plenty of basic suspension tuning principles on display.",
        "URL": "https://davemosstuning.com/2-clicks-out-tuono-fest-2/",
        "Tags": [
            "2 clicks out",
            "Aprilia",
            "how to",
            "Tuono"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "November 14th, 2018",
        "Title": "2 Clicks Out: Triumph Bobber Black",
        "Perex": " To understand just how visceral the Triumph Bobber Black can be, duckduckgo \"Steve McQueen Triumph Great Escape\" and watch what McQueen did with this bike in that movie.",
        "URL": "https://davemosstuning.com/2-clicks-out-triumph-bobber-black/",
        "Tags": [
            "how to",
            "street",
            "Suspension",
            "Triumph"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "November 12th, 2018",
        "Title": "Moss Unsprung: Tire Wear",
        "Perex": " Why is your tire tread worn down unevenly? What is that weird tearing pattern? Moss answers these questions and many more in this recording of a live stream show from the OnTheThrottle days. Its vintage video quality, but the info is priceless. This was top flight live streaming tech in olden times; you know, way [...]",
        "URL": "https://davemosstuning.com/moss-unsprung-tire-wear/",
        "Tags": [
            "street",
            "Suspension",
            "tires",
            "Track"
        ],
        "Categories": [
            "All",
            "How-To",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "November 8th, 2018",
        "Title": "2 Clicks Out: His \u0026 Hers Husqy 701",
        "Perex": " The Husqvarna 701 revived and reset the supermoto standard. Dave tunes one for a young lady at a riding school, and one for a gentleman at a Cycle Gear event.",
        "URL": "https://davemosstuning.com/2-clicks-out-his-hers-husqy-701/",
        "Tags": [
            "how to",
            "Husqvarna",
            "motard",
            "Track"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "November 6th, 2018",
        "Title": "Moss Unsprung: Top Out Springs",
        "Perex": " What the heck is a top out spring? Why should I care? Moss answers these questions and many more in this recording of a live stream show from the OnTheThrottle days. Its vintage video quality, but the info is priceless. This was top flight live streaming tech in olden times; you know, way back in [...]",
        "URL": "https://davemosstuning.com/moss-unsprung-top-out-springs/",
        "Tags": [
            "forks",
            "how to",
            "Suspension",
            "Ustream"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "November 5th, 2018",
        "Title": "Typical Morning With Dave Moss",
        "Perex": " We present here the typical morning in the life of Dave Moss. He tunes thousands of bikes a year. Here's a frenetic taste of what that looks like.",
        "URL": "https://davemosstuning.com/typical-morning-with-dave-moss/",
        "Tags": [
            "2 clicks out",
            "how to",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "October 31st, 2018",
        "Title": "2 Clicks Out: The FZ-07 Upgrade",
        "Perex": " The MT / FZ-07 is about as basic a motorcycle as can be purchased, particularly the suspension bits. Here's one properly outfitted, properly kitted.",
        "URL": "https://davemosstuning.com/2-clicks-out-the-fz-07-upgrade/",
        "Tags": [
            "FZ",
            "how to",
            "MT",
            "Track",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "October 29th, 2018",
        "Title": "2 Clicks Out: Mr \u0026 Ms Panigale 959",
        "Perex": " A pair of 2017 Ducati 959 Panigale's, one for her and one for him, rolled into Dave's suspension tuning chock.",
        "URL": "https://davemosstuning.com/2-clicks-out-mr-ms-panigale-959/",
        "Tags": [
            "2 clicks out",
            "Ducati",
            "Panigale",
            "Track"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "October 25th, 2018",
        "Title": "Throttle Application \u0026 Turn-In Points",
        "Perex": " Most people turn into corners too early (car or bike) and apply the throttle too late.",
        "URL": "https://davemosstuning.com/throttle-application-turn-in-points/",
        "Tags": [
            "how to",
            "street",
            "Suzuki"
        ],
        "Categories": [
            "All",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "October 24th, 2018",
        "Title": "AFM Round 7, Thunderhill Park October 20th and 21st, 2018",
        "Perex": "The last round of the year can have a lot of mixed emotions:- introspection, elation, anxiety, pride, confidence to list a few. For those in the hunt for a Championship you face the universal dilemma of  win it or bin it OR, get as many points as you can while keeping 5% in reserve. If you [...]",
        "URL": "https://davemosstuning.com/afm-round-7-thunderhill-park-october-20th-and-21st-2018/",
        "Tags": [
            "AFM",
            "R6",
            "racing",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Article",
            "Free",
            "Racing"
        ]
    },
    {
        "Date": "October 22nd, 2018",
        "Title": "Travel Chat: Fork Tube Cable Ties",
        "Perex": " America spent millions of dollars inventing a pen that could write in space. Russia used a pencil. In motorcycle suspension data that pencil is a cable tie on your fork tube (Zip Tie is a trademark so... well, you know the drill).",
        "URL": "https://davemosstuning.com/travel-chat-fork-tube-cable-ties/",
        "Tags": [
            "forks",
            "how to",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "October 18th, 2018",
        "Title": "Navigating A Combination Corner",
        "Perex": " Not to be confused with a Chicane (a flip flop interruption of track flow), a combination corner involves follow-on turns which result in a serious change of direction.",
        "URL": "https://davemosstuning.com/navigating-a-combination-corner/",
        "Tags": [
            "Fasterclass",
            "how to",
            "Track"
        ],
        "Categories": [
            "All",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "October 15th, 2018",
        "Title": "Contact Patch: Monitoring Tire Pressures",
        "Perex": " Tires = money, so why waste them. Using 3 brands of tires across 4 different horsepower motorcycles, Dave demonstrates how to track your tire pressures and temperatures through the morning so they don't shred themselves into the afternoon.",
        "URL": "https://davemosstuning.com/contact-patch-monitoring-tire-pressures/",
        "Tags": [
            "Contact Patch",
            "pressures",
            "temperatures",
            "tires"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "October 11th, 2018",
        "Title": "2 Clicks Out: Ninja H2 SX Sport Tourer",
        "Perex": " Kawasaki trimmed the blades of their Ninja H2 supercharger to create the first fire breathing sport touring dragon in history. Luckily the suspension and brakes are actually capable of handling it all. Darn the bad luck. Such misery, such strife. Sucks to be us.",
        "URL": "https://davemosstuning.com/2-clicks-out-ninja-h2-sx-sport-tourer/",
        "Tags": [
            "2 clicks out",
            "Kawasaki",
            "Ninja",
            "street"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "October 8th, 2018",
        "Title": "Shock Preload Adjusters",
        "Perex": " There are several types of motorcycle shock preload adjusters. Dave reviews them, explains how they work, and how he counts the adjustments he makes.",
        "URL": "https://davemosstuning.com/shock-preload-adjusters/",
        "Tags": [
            "sag",
            "Shocks",
            "street",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "October 4th, 2018",
        "Title": "4 Road Riding Skill Exercises",
        "Perex": " 1 Riding a road you do not know. 2 Coasting downhill (free wheeling in neutral). 3 Shadows late in the day. 4 Last ride of the day.",
        "URL": "https://davemosstuning.com/4-road-riding-skill-exercises/",
        "Tags": [
            "Acceleration",
            "Braking",
            "street",
            "Suzuki"
        ],
        "Categories": [
            "All",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "October 1st, 2018",
        "Title": "Contact Patch: Slick Tire Comet Tails",
        "Perex": " Slick tires lack sipes, commonly called tread, or tread pattern. This makes basic tire reading more difficult. But most slicks have core holes which can be read. The basic tell tale sign is the Comet Tail.",
        "URL": "https://davemosstuning.com/contac-patch-slick-tire-comet-tails/",
        "Tags": [
            "how to",
            "Suspension",
            "tires",
            "Track"
        ],
        "Categories": [
            "All",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "September 27th, 2018",
        "Title": "2 Clicks Out: KTM 1290 Super Duke",
        "Perex": " KTM 1290 Super Dukes are super rare, but we captured a couple, checked their vitals etc, and then released them back into the wild. They have no natural predators; like orcas that way.",
        "URL": "https://davemosstuning.com/2-clicks-out-ktm-1290-super-duke/",
        "Tags": [
            "Duke",
            "how to",
            "KTM",
            "street"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "September 24th, 2018",
        "Title": "Travel Chat: When To Change Fork Oil",
        "Perex": " How long would you go without changing your engine oil? Mr. Moss explains the particulars regarding Fork Oil. What do the Pro's think about fresh Fork Oil. Click the link. https://davemosstuning.com/graves-motorsports-suspension-technician-chris-lessing/",
        "URL": "https://davemosstuning.com/travel-chat-when-to-change-fork-oil/",
        "Tags": [
            "forks",
            "maintenance",
            "servicing",
            "street",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "September 24th, 2018",
        "Title": "CurbEater fork and shock oil heater test #2",
        "Perex": "September 9th, Thunderhill, California   Ambient temperature: 60F Fork Temperature 65F (effect of the morning sun) Shock temperature 58F   Session #1 at 0900 with no heating elements applied: Fork performance was normal in exaggerating bumps with the tire tracking harshly over sharp edged bumps for the first 3 laps, then the fork oil started to [...]",
        "URL": "https://davemosstuning.com/curbeater-fork-and-shock-oil-heater-test-2/",
        "Tags": [
            "forks",
            "how to",
            "Track",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Article",
            "Free",
            "Reviews"
        ]
    },
    {
        "Date": "September 20th, 2018",
        "Title": "Navigating A 90˚ On Camber Corner",
        "Perex": " Almost every track has an on-camber, low gear, 90-ish degree corner. Dave explains the braking and throttle techniques best suited to this type of bend in the road.",
        "URL": "https://davemosstuning.com/navigating-a-90%cb%9a-on-camber-corner/",
        "Tags": [
            "Fasterclass",
            "how to",
            "Track"
        ],
        "Categories": [
            "All",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "September 18th, 2018",
        "Title": "CurbEater suspension preheating equipment",
        "Perex": "Curbeater Test, Thunderhill Raceway Park: September 16th, 2018     Weather: 82F ambient high, 125F track high     Bike used: 2009 Yamaha R6/450 AFM ‘A’ race bike Ohlins 30mm NIX cartridges, Ohlins oil at 22 hours of track time Ohlins GP TTX rear shock, Ohlins oil at 22 hours track time         [...]",
        "URL": "https://davemosstuning.com/curbeater-suspension-preheating-equipment/",
        "Tags": [
            "forks",
            "how to",
            "Track"
        ],
        "Categories": [
            "All",
            "Article",
            "Free",
            "Reviews",
            "Testing Program"
        ]
    },
    {
        "Date": "September 18th, 2018",
        "Title": "Tire Testing: Bridgestone R11",
        "Perex": " Dave flogged the new Bridgestone R11 all day long, to within a pound of it's life. Pressures went up, pressures went down. Geometry tipped, and then tipped some more. Braking lines got fat, and then in shape. Cats and dogs living together... mass grip area.",
        "URL": "https://davemosstuning.com/tire-testing-bridgestone-r11/",
        "Tags": [
            "Bridgestone",
            "temperatures",
            "tires",
            "Track"
        ],
        "Categories": [
            "All",
            "Reviews",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "September 13th, 2018",
        "Title": "Yamaha R6 Data Dump 2006-15",
        "Perex": " Dave goes through the 2006-2015 Yamaha R6 from nose to tail detailing what needs to be modified/upgraded and what doesn't to reach optimum performance.",
        "URL": "https://davemosstuning.com/yamaha-r6-data-dump-2006-15/",
        "Tags": [
            "how to",
            "R6",
            "street",
            "Track",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Reviews",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "September 10th, 2018",
        "Title": "Test Ride: Panigale V4 Base Model",
        "Perex": " A kind gentleman allowed Dave to test ride his new base model Ducati Panigale V4. Dave evaluates the bike's braking, cornering, and acceleration, and concludes with some suspension direction.",
        "URL": "https://davemosstuning.com/test-ride-panigale-v4-base-model/",
        "Tags": [
            "Ducati",
            "Panigale",
            "settings",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Testing Program",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "September 7th, 2018",
        "Title": "3 Braking Techniques for the Road",
        "Perex": " Crashing is the worst way to slow down your motorcycle. Dave explains and demonstrates 3 better ways to reduce speed effectively and efficiently; Threshold braking, Trail braking, and Engine braking.",
        "URL": "https://davemosstuning.com/3-braking-techniques-for-the-road/",
        "Tags": [
            "Braking",
            "street",
            "Suzuki"
        ],
        "Categories": [
            "All",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "September 7th, 2018",
        "Title": "AFM Round 6, Sonoma Raceway, September 1st and 2nd 2018",
        "Perex": " Freemium Article.  Enjoy.  To view Premium content, please Subscribe or Login.  AFM ROUND 5, Sonoma Raceway September 1st  and 2nd Funny how some old or new songs pop in your head at particular moments……..“Three wheels on my wagon and I’m still rolling along….” Getting the trailer fixed by Marvin and Pete in Williams was [...]",
        "URL": "https://davemosstuning.com/afm-round-6-sonoma-raceway-september-1st-and-2nd-2018/",
        "Tags": [
            "AFM",
            "racing",
            "Track",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Article",
            "Free",
            "Racing"
        ]
    },
    {
        "Date": "September 4th, 2018",
        "Title": "2 Clicks Out: Sewing Machines",
        "Perex": " Never before have we had such capable, refined budget motorcycles; all the 250cc to 500cc single and twin engined sportbikes of our day. Awesome out of the crate for commuting, and easily upgraded for the twisties and track.",
        "URL": "https://davemosstuning.com/2-clicks-out-sewing-machines/",
        "Tags": [
            "2 clicks out",
            "how to",
            "Suspension",
            "Track"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "August 30th, 2018",
        "Title": "Fasterclass: Bounce \u0026 Set Compression",
        "Perex": " In this Fasterclass video, Dave Moss explains and demonstrates how to assess your motorcycle's Compression damping, and then set it correctly.",
        "URL": "https://davemosstuning.com/fasterclass-bounce-set-compression/",
        "Tags": [
            "Fasterclass",
            "forks",
            "how to",
            "settings"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "August 27th, 2018",
        "Title": "What is Remote Tuning with Dave Moss",
        "Perex": " Sometimes you just need Dave Moss in your tool box. Thankfully this is the 21st Century. Whether riding on the road or at the track, Dave is available to help. Here's an example experience.",
        "URL": "https://davemosstuning.com/what-is-remote-tuning-with-dave-moss/",
        "Tags": [
            "Remote Tuning",
            "settings",
            "street",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Suspension",
            "Testing Program",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "August 23rd, 2018",
        "Title": "Budget Suspension on Budget Motorbikes",
        "Perex": " Motorcycle prices continue to rise like everything else in a central banking society. Hence the Budget Bike to save the day... fitted with budget suspension, like the new Kawasaki Ninja 400.",
        "URL": "https://davemosstuning.com/budget-suspension-on-budget-motorbikes/",
        "Tags": [
            "forks",
            "Shocks",
            "street",
            "Suspension",
            "Track"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "August 20th, 2018",
        "Title": "Fasterclass: Shock Static Sag",
        "Perex": " Every bike must have static sag in the shock. Dave explains how to see if a bike has any, how to measure it, and how much you should have.",
        "URL": "https://davemosstuning.com/fasterclass-shock-static-sag/",
        "Tags": [
            "Fasterclass",
            "forks",
            "sag",
            "settings",
            "Shocks"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "August 15th, 2018",
        "Title": "Contact Patch: Rebound Tire Wear",
        "Perex": " Whether it's a touring tire, hypersport tire, or whatever tire, if it has sipes (tread grooves) they will tell you whether your rebound setting (front or rear) is too fast, too slow, or just right. You just need to learn to read... tires.",
        "URL": "https://davemosstuning.com/contact-patch-rebound-tire-wear/",
        "Tags": [
            "Contact Patch",
            "tires",
            "Track"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "August 13th, 2018",
        "Title": "Brake-Turn-Gas vs Brake-Gas-Turn",
        "Perex": " There's more than one way to skin a corner. Dave explains these techniques for getting into, through, and out of corners more efficiently (code for \"faster\").",
        "URL": "https://davemosstuning.com/brake-gas-turn/",
        "Tags": [
            "Acceleration",
            "Braking",
            "Fasterclass",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "August 11th, 2018",
        "Title": "ZX-6R 636 Fork Service 02-06",
        "Perex": " Dave performs a full fork service, compete with all the tricks to make the job easy and thorough, on the 2002 through 2006 Kawasaki ZX-6R 636 fork. This includes a new seal.",
        "URL": "https://davemosstuning.com/zx-6r-636-fork-service-02-06/",
        "Tags": [
            "forks",
            "how to",
            "Kawasaki",
            "servicing"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "August 8th, 2018",
        "Title": "2 Clicks Out: Front End Chatter",
        "Perex": " There are several common causes of front end chatter. Dave eliminates them one at a time for this rider, despite him crashing half way through the day.",
        "URL": "https://davemosstuning.com/2-clicks-out-front-end-chatter/",
        "Tags": [
            "forks",
            "Kawasaki",
            "settings",
            "Track"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "August 7th, 2018",
        "Title": "ZX-6R 636 Fork Oil Change 02-06",
        "Perex": " Dave goes through the process, step by step, of changing fork oil on the 2002 through 2006 Kawasaki ZX-6R (ZX636).  The over paid video editor made a mistake and put 02-05, but it does in fact include 2006.",
        "URL": "https://davemosstuning.com/zx-6r-fork-oil-change-02-05/",
        "Tags": [
            "forks",
            "Kawasaki",
            "maintenance",
            "servicing"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "August 2nd, 2018",
        "Title": "Fasterclass: Mapping Out Your Settings",
        "Perex": " The first step to improve your suspension setup (and thus the performance level of your bike), is to identify all your current settings, then record the changes you make. Dave demonstrates the methodology and then we follow him through a real world scenario. Forms to help you track your changes can be found on the [...]",
        "URL": "https://davemosstuning.com/fasterclass-mapping-out-your-settings/",
        "Tags": [
            "Fasterclass",
            "forks",
            "how to",
            "settings",
            "springs",
            "Track"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "July 30th, 2018",
        "Title": "Progressive vs Linear Springs",
        "Perex": " What's the difference between progressive and linear (straight rate) springs? Almost every bike sold today is fitted with progressive springs.",
        "URL": "https://davemosstuning.com/progressive-vs-linear-springs/",
        "Tags": [
            "forks",
            "Shocks",
            "springs",
            "street",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "July 26th, 2018",
        "Title": "2018 Hypermotard 939 SP Suspension Eval",
        "Perex": " Dave climbs (literally) aboard the 2018 Ducati Hypermotard 939 SP to see how it fits him, and then evaluates the suspension components \u0026 settings with 3 rider weight categories in mind.",
        "URL": "https://davemosstuning.com/2018-hypermotard-939-sp-suspension-eval/",
        "Tags": [
            "Ducati",
            "Hypermotard",
            "settings",
            "street"
        ],
        "Categories": [
            "All",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "July 23rd, 2018",
        "Title": "2 Clicks Out: 899 \u0026 959 Panigales",
        "Perex": " Dave does suspension setups on the Ducati 899 and 959 Panigale. Long live the V-twin, or as Ducati so fondly corrects the press, the L-Twin",
        "URL": "https://davemosstuning.com/2-clicks-out-899-959-panigales/",
        "Tags": [
            "Ducati",
            "Panigale",
            "settings",
            "street"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "July 20th, 2018",
        "Title": "How To: Adjust Single Sided Swingarm Chain",
        "Perex": " Dave adjusts the chain tension on a single sided swingarm with top bracing. There are 2 kinds of single sided swingarms, don'cha know.",
        "URL": "https://davemosstuning.com/how-to-adjust-single-sided-swingarm-chain/",
        "Tags": [
            "Chain",
            "maintenance",
            "settings",
            "street"
        ],
        "Categories": [
            "All",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "July 18th, 2018",
        "Title": "Motorcycle Shock Length",
        "Perex": " Altering motorcycle shock length is often called \"rear ride height adjustment.\" Most stock shocks are not adjustable for shock length. Some are. Why? And why should you care?",
        "URL": "https://davemosstuning.com/motorcycle-shock-length/",
        "Tags": [
            "Geometry",
            "Shocks",
            "street",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "July 16th, 2018",
        "Title": "2018 Diavel Carbon Suspension Eval",
        "Perex": " Dave evaluates the stock suspension components and settings on the 2018 Ducati Diavel Carbon, complete with recommendations for 3 weight categories, and how the bike fits him personally, or he fits the bike.",
        "URL": "https://davemosstuning.com/2018-diavel-carbon-suspension-eval/",
        "Tags": [
            "Diavel",
            "Ducati",
            "how to",
            "settings",
            "street"
        ],
        "Categories": [
            "All",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "July 13th, 2018",
        "Title": "GSX-R600 \u0026 750 Fork Service 06-10",
        "Perex": " Dave does a full service on the 2006 through 2010 GSX-R600 and GSX-R750 forks. Learn it, love it, live it.",
        "URL": "https://davemosstuning.com/gsx-r600-750-fork-service-06-10/",
        "Tags": [
            "forks",
            "GSXR",
            "servicing",
            "Suzuki"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "July 11th, 2018",
        "Title": "AFM Round 5, Thunderhill Raceway Park, July 7th and 8th, 2018",
        "Perex": " Freemium Article.  Enjoy.  To view Premium content, please Subscribe or Login.  AFM Round 5, Thunderhill Raceway Park July 7th and 8th Heading to the race event with a forecast of 104F for both days requires a lot of physical preparation. That means 1.5 gallons of water per day starting Tuesday to make sure your [...]",
        "URL": "https://davemosstuning.com/afm-round-5-thunderhill-raceway-park-july-7th-and-8th-2018/",
        "Tags": [
            "AFM",
            "racing",
            "Track",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Article",
            "Free",
            "Racing"
        ]
    },
    {
        "Date": "July 11th, 2018",
        "Title": "2 Clicks Out: the BMW R nineT",
        "Perex": " This boxer engine model has conventional forks instead of the Telelever system. The fuel tank even comes pre-dented to accommodate the forks at full lock. Suspension packages vary among the 5 editions available.",
        "URL": "https://davemosstuning.com/2-clicks-out-the-bmw-r-ninet/",
        "Tags": [
            "2 clicks out",
            "BMW",
            "settings",
            "street"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "July 9th, 2018",
        "Title": "Is Your Chain Worn Out?",
        "Perex": " Given a properly adjusted chain, Dave identifies the 3 symptoms of a worn out chain needing replacement. How quickly these signs manifest depends on the chain quality, your maintenance regimen, and of course, how hard you ride.",
        "URL": "https://davemosstuning.com/is-your-chain-worn-out/",
        "Tags": [
            "maintenance",
            "R6",
            "servicing",
            "street",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "July 6th, 2018",
        "Title": "2018 Panigale V4S  Suspension Eval",
        "Perex": " Dave evaluates the showroom suspension setup of the new 2018 Ducati Panigale V4S. While the hydraulics are adjusted electronically, the preload remains manual. He suggests settings for 3 weight categories in both dynamic and fixed modes. He also examines how the bike fits him. Finally, we toss in The Editor's Rant.",
        "URL": "https://davemosstuning.com/2018-panigale-v4s-suspension-eval/",
        "Tags": [
            "Ducati",
            "how to",
            "Panigale",
            "settings",
            "street"
        ],
        "Categories": [
            "All",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "July 4th, 2018",
        "Title": "Bleeding Upside Down Brake Calipers",
        "Perex": " Many standard and sportbike motorcycles feature the rear brake mounted underneath the swingarm. This creates difficulties in bleeding the air from the system.",
        "URL": "https://davemosstuning.com/bleeding-upside-down-brake-calipers/",
        "Tags": [
            "Braking",
            "maintenance",
            "servicing"
        ],
        "Categories": [
            "All",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "July 2nd, 2018",
        "Title": "Test Ride: 2018 FZ-10 / MT-10",
        "Perex": " We test the most recent Yamaha FZ-10 / MT-10 at Buttonwillow Raceway. Dave owns this bike in New Zealand where he spends his winters (their summers) and knows exactly how to tune it for our test rider Mike Pond. Then Dave does a quick tune of the same model for a street rider.",
        "URL": "https://davemosstuning.com/2018-fz-10-mt-10-test-ride/",
        "Tags": [
            "FZ",
            "MT",
            "settings",
            "Track",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Reviews",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "June 29th, 2018",
        "Title": "Clocking Brake Lines",
        "Perex": " Adjusting the angle of brake and clutch levers is a key fitment issue on every motorcycle. Achieving the optimum angle often involves \"clocking\" the brake line.",
        "URL": "https://davemosstuning.com/clocking-brake-lines/",
        "Tags": [
            "Braking",
            "maintenance",
            "settings",
            "street"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "June 27th, 2018",
        "Title": "AFM Round 4, Thunderhill Park June 16th and 17th, 2018",
        "Perex": " Freemium Article.  Enjoy.  To view Premium content, please Subscribe or Login.  AFM Round 4, Thunderhill Raceway Park, June 16th and 17th The second consecutive race event at Thunderhill is generally a faster one as riders are leveraging their experiences and gains from round 3 here just a few weeks ago. That requires everyone to [...]",
        "URL": "https://davemosstuning.com/afm-round-4-thunderhill-park-june-16th-and-17th-2018/",
        "Tags": [
            "AFM",
            "racing",
            "Track",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Article",
            "Free",
            "Racing"
        ]
    },
    {
        "Date": "June 27th, 2018",
        "Title": "2 Clicks Out: Moto Guzzi Fest",
        "Perex": " A flock of Moto Guzzi's flew into Dave's sport chock for tuning, a V7 II, a V7 III, and two 1200SE Griso's. There were some unique challenges among the common suspension setup issues.",
        "URL": "https://davemosstuning.com/2-clicks-out-moto-guzzi-fest/",
        "Tags": [
            "how to",
            "Moto Guzzi",
            "settings",
            "street"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "June 18th, 2018",
        "Title": "2018 Ducati Scrambler 1100 Showroom Suspension Eval",
        "Perex": " Dave takes a look at how the 2018 Ducati Scrambler 1100 fits him, then evaluates the suspension components, their showroom settings, and suggests settings for 3 rider weight categories.",
        "URL": "https://davemosstuning.com/2018-ducati-scrambler-1100-showroom-suspension-eval/",
        "Tags": [
            "Ducati",
            "how to",
            "settings",
            "street"
        ],
        "Categories": [
            "All",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "June 13th, 2018",
        "Title": "How To: Stuck Brake Caliper Pistons",
        "Perex": " Dave trouble shoots a brake caliper with some stuck pistons, and demonstrates proper piston behavior when they're all working as designed.",
        "URL": "https://davemosstuning.com/how-to-stuck-brake-caliper-pistons/",
        "Tags": [
            "Braking",
            "maintenance",
            "servicing",
            "street"
        ],
        "Categories": [
            "All",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "June 11th, 2018",
        "Title": "2 Clicks Out: Yamaha R1M quick tune",
        "Perex": " A Yamaha R1M rider was unexpectedly pleased to see Dave at a Yamaha sponsored track day and took the opportunity to have Dave do a basic suspension setup for his carbon'd out R1M.",
        "URL": "https://davemosstuning.com/2-clicks-out-yamaha-r1m-quick-tune/",
        "Tags": [
            "2 clicks out",
            "how to",
            "settings",
            "Track",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "June 6th, 2018",
        "Title": "FasterClass: Bounce \u0026 Set Rebound",
        "Perex": " We see Dave bounce bikes, then turn rebound and compression screws, and wish we could have a mini Dave in our tool box. In this video he teaches how to bounce the bike properly, and based on the results, set the rebound correctly. (From the Lawyers: as with any suspension change, ride moderately until you [...]",
        "URL": "https://davemosstuning.com/fasterclass-bounce-set-rebound/",
        "Tags": [
            "Fasterclass",
            "forks",
            "how to",
            "settings",
            "Shocks"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "June 4th, 2018",
        "Title": "2 Clicks Out: Setup For 2 Up",
        "Perex": " One of the top 5 questions for Dave is settings for 2 up rides. He does one here, including tire pressure. While it's on a Ducati Monster, the principles are the same for any bike.",
        "URL": "https://davemosstuning.com/2-clicks-out-setup-for-2-up/",
        "Tags": [
            "2 clicks out",
            "Ducati",
            "settings",
            "street"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "May 31st, 2018",
        "Title": "Test Ride: 2018 Yamaha R6",
        "Perex": " Yamaha Champions Riding School brought some official Yamaha demo bikes out for test rides and we were lucky enough to snag the opportunity. Our second bike was the 2018 R6, which Dave Moss tested as only he can.",
        "URL": "https://davemosstuning.com/test-ride-2018-yamaha-r6/",
        "Tags": [
            "2 clicks out",
            "R6",
            "settings",
            "Track",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Reviews",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "May 30th, 2018",
        "Title": "AFM Round 3, Thunderhill Raceway Park, May 26 \u0026 27, 2018",
        "Perex": " Freemium Article.  Enjoy.  To view Premium content, please Subscribe or Login.  When you wake up at 2am Saturday morning with rain tapping your trailer roof repeatedly, you tend to feel a little bit disoriented. Wait:- this is California in May and I’m hearing rain? So much for the greatest job in the world – [...]",
        "URL": "https://davemosstuning.com/afm-round-3-thunderhill-raceway-park-may-26-27-2018/",
        "Tags": [
            "AFM",
            "racing",
            "Track",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Article",
            "Free",
            "Racing"
        ]
    },
    {
        "Date": "May 29th, 2018",
        "Title": "Ask Dave: Brake \u0026 Clutch Lever Shapes",
        "Perex": " We see all sorts of sizes and shapes of these levers bike to bike, even side to side on the same bike. And then there's the whole aftermarket with shorty levers, etc blah etc. Dave explains what to look for in evaluating and adjusting what you have, and what to look for if you're shopping [...]",
        "URL": "https://davemosstuning.com/ask-dave-brake-clutch-lever-shapes/",
        "Tags": [
            "ergonomics",
            "street",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "May 24th, 2018",
        "Title": "FasterClass: Ultimate Setting Sag Tutorial",
        "Perex": " We realize that no matter what we produce there will always be questions, and understandably so. But we did our darnedest to cover everything in this video.",
        "URL": "https://davemosstuning.com/fasterclass-ultimate-setting-sag-tutorial/",
        "Tags": [
            "Fasterclass",
            "how to",
            "sag",
            "settings"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "May 22nd, 2018",
        "Title": "Test Ride: 2018 Yamaha R1",
        "Perex": " Yamaha Champions Riding School brought some official Yamaha demo bikes out for test rides and we were lucky enough to snag the opportunity. We start with the 2018 R1 and test it Dave Moss style.",
        "URL": "https://davemosstuning.com/test-ride-2018-yamaha-r1/",
        "Tags": [
            "how to",
            "settings",
            "Track",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Reviews",
            "Suspension",
            "Testing Program",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "May 16th, 2018",
        "Title": "2 Clicks Out: Bike Nite Triumph 675’s",
        "Perex": " Dave tunes a bevy of Triumph 675's, a bike of many faces and styles, each with it's own quirks, but all similar in their basic needs.",
        "URL": "https://davemosstuning.com/2-clicks-out-bike-nite-triumph-675s/",
        "Tags": [
            "how to",
            "settings",
            "sportbike",
            "street",
            "Triumph"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "May 14th, 2018",
        "Title": "2 Clicks Out: Shock Static Sag",
        "Perex": " Static or Free Sag is more critical for the rear of the bike than for the front because you're essentially sitting atop the shock. Here we present the full shock static sag spectrum; none, some, just right, and too much.",
        "URL": "https://davemosstuning.com/2-clicks-out-shock-static-sag/",
        "Tags": [
            "2 clicks out",
            "sag",
            "settings",
            "Shocks"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "May 9th, 2018",
        "Title": "2 Clicks Out: Shock Rebound Too Fast",
        "Perex": " When shock rebound is out of adjustment it is most commonly too fast, for 2 reasons. First, it seems the factory shock rebound settings of most bikes off the showroom floor is set too fast to begin with. Why? Nobody knows. Second, as the shock ages with mileage the oil thins which speeds up the [...]",
        "URL": "https://davemosstuning.com/2-clicks-out-shock-rebound-too-fast/",
        "Tags": [
            "2 clicks out",
            "how to",
            "settings",
            "Shocks"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "May 7th, 2018",
        "Title": "Ask Dave: Geometry \u0026 Environment",
        "Perex": " A street environment has different demands from a track environment. You can get a better ride by optimizing suspension setup and geometry for each. For example, track setups can be torture on the street, and street setups horribly inadequate (even dangerous) for the track.",
        "URL": "https://davemosstuning.com/ask-dave-geometry-environment/",
        "Tags": [
            "Geometry",
            "settings",
            "street",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "May 2nd, 2018",
        "Title": "Contact Patch: Reading Slick Tires 1",
        "Perex": " Many riders become track day junkies after enjoying their first few days at the track. The limitations of street tires, even hypersport tires, quickly becomes apparent and riders switch to slicks. And of course, club, semi pro, and pro racers all ride slicks.",
        "URL": "https://davemosstuning.com/contact-patch-reading-slick-tires-1/",
        "Tags": [
            "Contact Patch",
            "tires",
            "Track"
        ],
        "Categories": [
            "All",
            "How-To",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "April 30th, 2018",
        "Title": "Track Day Bike Prep",
        "Perex": " Heading to the track with your street bike? Lotsa riders do just that. Dave does a basic bike prep for the average trackday using a Yamaha R6 as the example. Check with your trackday organizer just to be sure they don't have some bizarre requirement beyond what's covered here. We end the video with Dave [...]",
        "URL": "https://davemosstuning.com/track-day-bike-prep/",
        "Tags": [
            "how to",
            "settings",
            "Track",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "April 29th, 2018",
        "Title": "Dave Moss Tuning: race day data sheet",
        "Perex": " Freemium Article.  Enjoy.  To view Premium content, please Subscribe or Login.  DAVE MOSS TUNING Race Day Data sheet RACE ORG: ______________________________ RACE #: _____________ BIKE: 1. __________________ 2. ______________ 3. ___________________ TRACK:__________________________________ AMBIENT TEMPS 9am:______            AMBIENT TEMPS 1pm: ______ TRACK TEMPS 9am:______                 AMBIENT TEMPS 1pm: ______ TIRE GAUGE CALIBRATED ON ___/___/___ Correction Factor [...]",
        "URL": "https://davemosstuning.com/dave-moss-tuning-race-day-data-sheet/",
        "Tags": [
            "how to",
            "racing",
            "settings"
        ],
        "Categories": [
            "All",
            "Article",
            "Free"
        ]
    },
    {
        "Date": "April 29th, 2018",
        "Title": "DMT Track Day data tracking sheet",
        "Perex": " Freemium Article.  Enjoy.  To view Premium content, please Subscribe or Login.  DAVE MOSS TUNING: Track Day Data Sheet TRACK ORG: ______________________________ SESSION #: _____________ BIKE: 1. __________________ 2. ______________ 3. ___________________ TRACK:__________________________________ AMBIENT TEMP:_____________              TRACK TEMP:___________ FRONT TIRE: MAKE:______________ SIZE: ___________ COMPOUND: ________ COLD:_______________ HOT: ___________ GAIN: ______________  TEMP: _________ REAR TIRE: MAKE:______________ SIZE: [...]",
        "URL": "https://davemosstuning.com/dmt-track-day-data-tracking-sheet/",
        "Tags": [
            "how to",
            "settings",
            "Track"
        ],
        "Categories": [
            "All",
            "Article",
            "Free"
        ]
    },
    {
        "Date": "April 25th, 2018",
        "Title": "How-To: Adjusting Aftermarket Rearsets (foot rests)",
        "Perex": " We finish up the Attack Performance rearset install by adjusting the rearsets to suit the rider, in this case super squid Dave Williams. We also point out some of the design features common to aftermarket rearsets in general and also the Attack rearsets specifically.",
        "URL": "https://davemosstuning.com/how-to-adjusting-aftermarket-rearsets-foot-rests/",
        "Tags": [
            "ergonomics",
            "how to",
            "settings"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "April 25th, 2018",
        "Title": "Tuned Suspension = Speed",
        "Perex": " It's Brandon Souza's first race weekend. While he's no novice rider, he's no Marquez either. His suspension is out of adjustment. His lap times plummet as Dave gets to work.",
        "URL": "https://davemosstuning.com/tuned-suspension-speed/",
        "Tags": [
            "racing",
            "settings",
            "temperatures",
            "tires",
            "Track"
        ],
        "Categories": [
            "All",
            "Racing",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "April 23rd, 2018",
        "Title": "2 Clicks Out: XDiavel vs V-Rod",
        "Perex": " During a bike nite club event we encountered a Ducati XDiavel and Harley-Davidson V-Rod. Sort of a cruiser suspension showdown, or suspension smackdown-ish kind of thingy.",
        "URL": "https://davemosstuning.com/2-clicks-out-xdiavel-vs-v-rod/",
        "Tags": [
            "cruiser",
            "Ducati",
            "settings",
            "street"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 18th, 2018",
        "Title": "Contact Patch: Reading Street Tires 1",
        "Perex": " The tires never lie. Dave reads the tire wear of several street bikes, explains the malady causing the wear, and the remedy necessary to restore tire health, which ultimately extends tire life... which invigorates bank balance stamina.",
        "URL": "https://davemosstuning.com/contact-patch-reading-street-tires-1/",
        "Tags": [
            "settings",
            "temperatures",
            "tires",
            "Track"
        ],
        "Categories": [
            "All",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "April 18th, 2018",
        "Title": "AFM Round Two at Button Willow Raceway, April 14th and 15th, 2018",
        "Perex": " 2018 Sponsors: BBJK, Bell Helmets, Bridgestone, Driven USA, Galfer, GP Frame and Wheel, GP Sports, Insurrection racing, KC Paint, Keigwin's at the Track, MC Tech, Motion Pro, MOTO-D Racing, Motul, M Works, NorCal HRT, Optimal bodywork, Oxymoron Photography, RAM Print, Serious R\u0026D, Zooni leathers. New for 2018: Y2Wheels (https://www.y2wheels.com/shop/) HELP NEEDED: I am looking for [...]",
        "URL": "https://davemosstuning.com/afm-round-two-at-button-willow-raceway-april-14th-and-15th-2018/",
        "Tags": [
            "AFM",
            "racing",
            "Track",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Article",
            "Free",
            "Racing"
        ]
    },
    {
        "Date": "April 17th, 2018",
        "Title": "AFM Round 1 at Button Willow Raceway, March 16-18, 2018",
        "Perex": " The 2018 Season rolls around again in very short order. Many sponsors are back for another year after three Championships secured in 2017 (Formula 40 lightweight, 450 Superbike, 450 Superstock). 2018 Sponsors: BBJK, Bell Helmets, Bridgestone, Driven USA, Galfer, GP Frame and Wheel, GP Sports, Insurrection racing, KC Paint, Keigwin's at the Track, MC Tech, [...]",
        "URL": "https://davemosstuning.com/afm-round-1-at-button-willow-raceway-march-16-18-2018/",
        "Tags": [
            "AFM",
            "racing",
            "Track",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Article",
            "Free",
            "Racing"
        ]
    },
    {
        "Date": "April 17th, 2018",
        "Title": "Ask Dave: Ergonomics",
        "Perex": " Dave addresses hand, feet, and leg ergonomics. Even the most average motorcycle can be custom fit to suit the rider, something restricted to all but the most expensive models in the car world.",
        "URL": "https://davemosstuning.com/ask-dave-ergonomics/",
        "Tags": [
            "ergonomics",
            "settings",
            "street",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "April 15th, 2018",
        "Title": "Fork Oil Change of GP Suspension 25mm Kit",
        "Perex": " Start from the very beginning of writing settings down, then how to remove components one at a time to get the forks apart. Understand how to drain the oil, bleed and reset new oil levels and then reassemble the fork.",
        "URL": "https://davemosstuning.com/fork-oil-change-of-gp-suspension-25mm-kit/",
        "Tags": [
            "forks",
            "how to",
            "maintenance",
            "servicing"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 14th, 2018",
        "Title": "How-To: Brembo Brake Caliper Cleaning",
        "Perex": " Go step by step with Dave as he cleans the Brembo calipers on the Aprilia RSV4R Factory LE. This technique can be used with any caliper, but obviously the way brake pads are installed and removed will be very different with other manufacturers.",
        "URL": "https://davemosstuning.com/how-to-brembo-brake-caliper-cleaning/",
        "Tags": [
            "Braking",
            "how to",
            "maintenance",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "April 14th, 2018",
        "Title": "How-To: Removing \u0026 Reinstaling A Front Wheel Correctly",
        "Perex": " With radial forks it is simply not good enough to just bolt everything together. Careless work can result in binding wheels, out of alignment calipers and boiling brake fluid. If you want to make sure the job is done right, follow along with Dave as he uses the RSV4R as an example.",
        "URL": "https://davemosstuning.com/how-to-removing-reinstaling-a-front-wheel-correctly/",
        "Tags": [
            "how to",
            "maintenance",
            "street"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "April 14th, 2018",
        "Title": "2 Clicks Out: AFM Racer 842",
        "Perex": " In this episode of Two Clicks Out for the track, AFM racer Max Sawicky #842 gets suspension tuning to cope with Sonoma Raceway in northern California. Max races the 450 Superbike class.",
        "URL": "https://davemosstuning.com/2-clicks-out-afm-racer-842/",
        "Tags": [
            "2 clicks out",
            "AFM",
            "racing",
            "settings",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 12th, 2018",
        "Title": "Yamaha FZ-07 \u0026 FZ-09 suspension comparison",
        "Perex": " Different price points involve differences beyond just the number of cc's and their resulting ponies at the rear wheel. Different price points almost always involve suspension mods to cope with said pony changes. Here we compare the suspension and tuning options of the Yamaha FZ/MT 07 vs FZ/MT 09.",
        "URL": "https://davemosstuning.com/yamaha-fz-07-fz-09-suspension-comparison/",
        "Tags": [
            "FZ",
            "settings",
            "street",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 10th, 2018",
        "Title": "Contact Patch: Cold Tire Pressure",
        "Perex": " Like most things we discuss on this channel, the faster you ride the more important they become. Cold tire pressure is one of those thingy's. It becomes important to faster street riders, and to track day riders who don't use tire warmers... yet.",
        "URL": "https://davemosstuning.com/contact-patch-cold-tire-pressure/",
        "Tags": [
            "Contact Patch",
            "temperatures",
            "tires"
        ],
        "Categories": [
            "All",
            "Free",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "April 5th, 2018",
        "Title": "2017 Yamaha SCR950 Stock Suspension Eval",
        "Perex": " We evaluate the 2017 Yamaha SCR950 stock suspension settings off the showroom floor including settings for 3 rider weight ranges. Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2017-yamaha-scr950-stock-suspension-eval/",
        "Tags": [
            "settings",
            "standard",
            "street",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 4th, 2018",
        "Title": "2 Clicks Out: Tiffany Webb, USBA racer",
        "Perex": " Tiffany Webb races a 2010 Yamaha R6 in the Masters of the Mountains series in Utah. The bike has issues, but Dave has the therapy it needs.",
        "URL": "https://davemosstuning.com/2-clicks-out-tiffany-webb-usba-racer/",
        "Tags": [
            "2 clicks out",
            "R6",
            "racing",
            "Track"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "2 Clicks Out: ZX-10R check up",
        "Perex": " Dave first tuned this 2016 Kawasaki ZX-10R shortly after the owner bought it new from the dealership. You'll notice that some adjustments are still good from the last time he tuned it, and others are not. The question is why. The answer to one of the adjustments should be easy based on obvious clues in [...]",
        "URL": "https://davemosstuning.com/2-clicks-out-zx-10r-check-up/",
        "Tags": [
            "how to",
            "Kawasaki",
            "settings",
            "Track",
            "ZX-10"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Contact Patch: Testing Tire Pressures",
        "Perex": " Tire pressure has the single greatest effect on tire wear and grip. It's the first line on the trouble shooting flow chart. Dave deliberately alters his rear tire pressure to demonstrate what happens and why.",
        "URL": "https://davemosstuning.com/contact-patch-testing-tire-pressures/",
        "Tags": [
            "Contact Patch",
            "temperatures",
            "tires",
            "Track"
        ],
        "Categories": [
            "All",
            "How-To",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Ask Dave: Hello Brake Fluid",
        "Perex": " You'd think by the year 2018 motorcycle brake fluid would be more better than it is. Oh well. Of course, like most things, the faster you go the more critical it becomes. Wax is not as crucial to the 4 days per year snowboarder as it is to Shaun White or Chloe Kim.",
        "URL": "https://davemosstuning.com/ask-dave-hello-brake-fluid/",
        "Tags": [
            "Braking",
            "maintenance",
            "servicing",
            "street"
        ],
        "Categories": [
            "All",
            "Free",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Bottomed Out Forks",
        "Perex": " You can bottom out your forks in several ways. In this video we discover the solution adequate in one situation is inadequate in another. Life on 2 wheels does require more diligence, intelligence, and skill. You're up to the task.",
        "URL": "https://davemosstuning.com/bottomed-out-forks/",
        "Tags": [
            "forks",
            "settings",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Chain Slack",
        "Perex": " Dave covers the general principles of chain slack; why for, how much, etc etc.",
        "URL": "https://davemosstuning.com/chain-slack/",
        "Tags": [
            "Chain",
            "maintenance",
            "settings",
            "street"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "2 Clicks Out: 2017 Yamaha R6",
        "Perex": " Dave tunes a brandy new 2017 Yamaha R6 with about 600 miles on it, and still shod with its stock tires. It's like a wee little R1... maybe?",
        "URL": "https://davemosstuning.com/2-clicks-out-2017-yamaha-r6/",
        "Tags": [
            "2 clicks out",
            "R6",
            "settings",
            "street",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Fork Service Yamaha R6 2006-07",
        "Perex": " This is a very comprehensive step by step video on the entire procedure for the oil and seal change and tools needed.",
        "URL": "https://davemosstuning.com/fork-service-yamaha-r6-2006-07/",
        "Tags": [
            "forks",
            "R6",
            "servicing",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Superbike Comparison 2017",
        "Perex": " By \"Superbike\" we mean all the liter bike versions created for all national and World Superbike racing; the Kawasaki ZX-10RR, Suzuki GSX-R1000R, Honda CBR1000RR SP, and Yamaha R1M. Dave has actually tuned R1M's at the track and you can email him for more granular details about that bike.",
        "URL": "https://davemosstuning.com/superbike-comparison-2017/",
        "Tags": [
            "ergonomics",
            "settings",
            "sportbikes",
            "street"
        ],
        "Categories": [
            "All",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "How-To: Setting Brake Calipers",
        "Perex": " You've changed your brake pads and proceed to reinstall your brake calipers, i.e. mount them back onto the forks. Is there a process involved? Why, yes, there is.",
        "URL": "https://davemosstuning.com/how-to-setting-brake-calipers/",
        "Tags": [
            "Braking",
            "maintenance",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Fork Oil Change ZX-6R 09-15",
        "Perex": " Dave changes the fork oil step by step on the 2009-2015 Kawasaki ZX-6R fork. It's a unique fork with a couple of tricks to it which Dave explains fully.",
        "URL": "https://davemosstuning.com/fork-oil-change-zx-6r-09-15/",
        "Tags": [
            "forks",
            "Kawasaki",
            "servicing",
            "sportbike"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Saving You From…",
        "Perex": " This video could have been titled, \"Too Many Lawyers.\" (For Commonwealthers, a \"lawyer\" is a solicitor. In America a \"solicitor\" is a door-to-door salesman.) It could also be titled, \"Recalcitrant Duc ,\" or \"Peevish Panigale.\"",
        "URL": "https://davemosstuning.com/saving-you-from/",
        "Tags": [
            "Ducati",
            "Panigale",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Fork Service ZX-6R 09-15",
        "Perex": " Dave guides us through a fork service with seal replacement on the 2009-2015 Kawasaki ZX-6R. There are a few unique aspects to this fork, and he makes them plain and simple.",
        "URL": "https://davemosstuning.com/fork-service-zx-6r-09-15/",
        "Tags": [
            "forks",
            "Kawasaki",
            "servicing",
            "sportbike"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Ask Dave: Motorcycle Maintenance",
        "Perex": " With just 2 wheels and no cage, there are more things to maintain on a motorcycle than engine oil.",
        "URL": "https://davemosstuning.com/ask-dave-motorcycle-maintenance/",
        "Tags": [
            "maintenance",
            "sportbike",
            "street",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "2 Clicks Out: Aprilia RSV4 Fest",
        "Perex": " We have a new Aprilia RSV4 RR sporting a new shock, a new RSV4 RF all stock, and a 2014 RSV4 just because, like Mt Everest, it was there. Several tire gremlins come into play as well.",
        "URL": "https://davemosstuning.com/2-clicks-out-aprilia-rsv4-fest/",
        "Tags": [
            "Aprilia",
            "settings",
            "sportbike",
            "Suspension",
            "Track"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Stock R6 Shock Upgrade \u0026 Swap 2008-16",
        "Perex": " Used R6 shocks are cheap, and inexpensive to refurbish and upgrade (GP Suspension did this one). Dave races them over expensive aftermarket shocks. But the install on the 2008-16 R6 is more complex than other bikes.",
        "URL": "https://davemosstuning.com/stock-r6-shock-upgrade-swap-2008-16/",
        "Tags": [
            "R6",
            "racing",
            "Shocks",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "FZ-09 FJ-09 XSR900 Comparison",
        "Perex": " Yamaha offers 2 additional versions of their base model FZ-09 (MT-09 in Europe), the FJ-09 and the XSR900. We decided to compare them back to back, front to front, side to side, etc to etc. Looks aren't the only difference.",
        "URL": "https://davemosstuning.com/fz-09-fj-09-xsr900-comparison/",
        "Tags": [
            "ergonomics",
            "settings",
            "standard",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Ask Dave: Engine Size?",
        "Perex": " What size bike should you buy? What are the advantages and disadvantages of 300's 600's and 1000's.",
        "URL": "https://davemosstuning.com/ask-dave-engine-size/",
        "Tags": [
            "sportbike",
            "street",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Safety Wire \u0026 Tech Prepping a Bike",
        "Perex": " Dave safety wires and tech preps a 600 to be raced in the AFM, Northern California's club racing organization. These procedures and processes are common to all club racing orgs on the planet, and what you see here will get you 98% of the way there. Consult your club's rule book to tidy up any [...]",
        "URL": "https://davemosstuning.com/safety-wire-tech-prepping-a-bike/",
        "Tags": [
            "racing",
            "sportbike",
            "Track"
        ],
        "Categories": [
            "All",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Safety Wire Pliers",
        "Perex": " Veteran mechanic, Greg Bunting, at Mach 1 Motorsports in Vallejo, CA loaned us his pair of safety wire pliers for one of our video projects. The design of the tool hasn't changed in 50 years.",
        "URL": "https://davemosstuning.com/safety-wire-pliers-2/",
        "Tags": [
            "racing",
            "sportbike",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "2 Clicks Out: Racing a bone stock R6",
        "Perex": " Dave tunes a bone stock 2009 Yamaha R6 ('09 - '14 are all the same) for an amateur racer at the end of the 2015 season, and then again at a different track for the first round of 2016. While a fully adjustable shock and forks are better than non adjustable ones, stock components are [...]",
        "URL": "https://davemosstuning.com/2-clicks-out-racing-a-bone-stock-r6/",
        "Tags": [
            "2 clicks out",
            "how to",
            "R6",
            "racing"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Ask Dave: Front End Geometry",
        "Perex": " We turn now to a review of the Bridgestone family of sport tires. It's a picture perfect family, all members beautiful, talented, and intelligent.",
        "URL": "https://davemosstuning.com/ask-dave-front-end-geometry/",
        "Tags": [
            "Geometry",
            "how to",
            "street",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Contact Patch: Bridgestone Tire Family",
        "Perex": " We turn now to a review of the Bridgestone family of sport tires. It's a picture perfect family, all members beautiful, talented, and intelligent.",
        "URL": "https://davemosstuning.com/contact-patch-bridgestone-tire-family/",
        "Tags": [
            "Contact Patch",
            "Geometry",
            "tires"
        ],
        "Categories": [
            "All",
            "Reviews",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "2 Clicks Out: Titanium shock spring",
        "Perex": " Why are all fork and shock springs made of steel and not some super exotic metal? Dave knows. DMT2 Channel: get all of Dave's premium content for one low subscription price.",
        "URL": "https://davemosstuning.com/2-clicks-out-titanium-shock-spring/",
        "Tags": [
            "2 clicks out",
            "Shocks",
            "springs"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "2017 Ducati SuperSport Test",
        "Perex": " This is our most complete bike review to date, complete with test ride and Dave's suspension changes afterwards. Plus a suspension setup for a customer who had just purchased one. We also do a comparison with the 1299 Panigale.",
        "URL": "https://davemosstuning.com/2017-ducati-supersport-test/",
        "Tags": [
            "Ducati",
            "settings",
            "standard",
            "street",
            "Suspension"
        ],
        "Categories": [
            "All",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Ask Dave: Rear End Geometry",
        "Perex": " As with anything sportbikes, the faster you ride the more it matters. If you're into modifying your bike, the more it matters. Key phrase: swing arm angle.",
        "URL": "https://davemosstuning.com/ask-dave-rear-end-geometry/",
        "Tags": [
            "Geometry",
            "Shocks",
            "street",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Fork Service Yamaha R1 09-14",
        "Perex": " We do a complete fork service for the 2009 to 2014 Yamaha R1. Dave takes you through it step by step revealing a number of tips that make the task even smoother. Fear not, the Moss is with you. Always. Spies won the World Superbike Championship aboard the '09 model and Yamaha dominated American Superbike [...]",
        "URL": "https://davemosstuning.com/fork-service-yamaha-r1-09-14/",
        "Tags": [
            "forks",
            "servicing",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Telescopic Fork Seal Issues",
        "Perex": " Many new bikes are still equipped with Telescopic Forks, usually on budget minded models. Dave offers some pointers to help maintain them.",
        "URL": "https://davemosstuning.com/telescopic-fork-seal-issues/",
        "Tags": [
            "forks",
            "maintenance",
            "servicing"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Tire and Suspension Tuning Chats",
        "Perex": " A compilation of random chats with riders who've got a question... about tires, suspension, riding techniques, strange noises, etc etc.",
        "URL": "https://davemosstuning.com/tire-and-suspension-tuning-chats/",
        "Tags": [
            "street",
            "Suspension",
            "tires",
            "Track"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Ask Dave: Brake Timing",
        "Perex": " Different environments have different demands. See a cop. Red light. Deer X-ing. Approaching each of 12 turns on your fave set o'twisties.",
        "URL": "https://davemosstuning.com/ask-dave-brake-timing/",
        "Tags": [
            "Braking",
            "street",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Fork Service Ohlins Road \u0026 Track",
        "Perex": " Dave performs a full service on the popular Ohlins Road \u0026 Track fork. It's not rocket surgery, it's Swedish. You'll want a summer cottage there afterwards for sure.",
        "URL": "https://davemosstuning.com/fork-service-ohlins-road-track/",
        "Tags": [
            "forks",
            "servicing"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Ask Dave: Brake Pressure",
        "Perex": " Not enough and you run wide. Too much and you low side. Like Goldilocks, it needs to be just right.",
        "URL": "https://davemosstuning.com/ask-dave-brake-pressure/",
        "Tags": [
            "Braking",
            "street",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Graves Motorsports Suspension Technician Chris Lessing part 2",
        "Perex": " We speak with Graves Motorsports Suspension Technician Chris Lessing about his ritual of suspension service each Thursday before every AMA race weekend. Graves Motorsports spearheads the Factory Yamaha effort, but also supports many other bikes in competition.",
        "URL": "https://davemosstuning.com/graves-motorsports-suspension-technician-chris-lessing-part-2/",
        "Tags": [
            "racing",
            "Suspension"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "2 Clicks Out: Tri-Axis Comparison",
        "Perex": " We've put together a tri-axis suspension tuning comparison here. X axis: a 2015 Aprilia RSV4 and 2015 Yamaha R1 Y axis: the R1 showroom settings and R1 settings that work Z axis: the brand new 2015's and a 2002 R6, bling or budget.",
        "URL": "https://davemosstuning.com/2-clicks-out-tri-axis-comparison/",
        "Tags": [
            "Aprilia",
            "settings",
            "sportbike",
            "Track",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "2 Clicks Out: Dead Preload Adjuster",
        "Perex": " Some times the old ways are the best ways. In this case, the remote preload adjuster on a shock has failed. Now what?",
        "URL": "https://davemosstuning.com/2-clicks-out-dead-preload-adjuster/",
        "Tags": [
            "maintenance",
            "settings",
            "Shocks"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Graves Motorsports Suspension Technician Chris Lessing part 1",
        "Perex": " We speak with Graves Motorsports Suspension Technician Chris Lessing about his ritual of suspension service each Thursday before every AMA race weekend. Graves Motorsports spearheads the Factory Yamaha effort, but also supports many other bikes in competition.",
        "URL": "https://davemosstuning.com/graves-motorsports-suspension-technician-chris-lessing/",
        "Tags": [
            "forks",
            "maintenance",
            "racing",
            "servicing",
            "Shocks"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Contact Patch: Healing a shredded tire",
        "Perex": " A rider at a track day began shredding a brand new set of tires. Via texting, he used Dave's remote tuning service to heal the rear tire, and saved it to ride another day",
        "URL": "https://davemosstuning.com/contact-patch-healing-a-shredded-tire/",
        "Tags": [
            "Contact Patch",
            "Geometry",
            "temperatures",
            "tires",
            "Track"
        ],
        "Categories": [
            "All",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Ask Dave: Brake Duration",
        "Perex": " Just how long should / could / would you squeeze that brake lever. Long and light? Quick and hard? Dave knows",
        "URL": "https://davemosstuning.com/ask-dave-brake-duration/",
        "Tags": [
            "Braking",
            "street",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "How-To: Installing Rear Brake Pads",
        "Perex": " Next is a brake pad install for our Suzuki GSX-R750 project bike.",
        "URL": "https://davemosstuning.com/how-to-installing-rear-brake-pads/",
        "Tags": [
            "Braking",
            "maintenance",
            "servicing",
            "street"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "2 Clicks Out: The Lonesome 750",
        "Perex": " Suzuki stands alone in the 750cc category, and has for years. In this video Dave tunes a pair of GSX-R750's. One for a gentleman at his first riding school, and the other for a Trackday veteran; both ends of the spectrum, novice and expert.",
        "URL": "https://davemosstuning.com/2-clicks-out-the-lonesome-750/",
        "Tags": [
            "settings",
            "sportbike",
            "Suzuki"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Scrambler Desert Sled \u0026 Cafe Racer",
        "Perex": " Dave compares them side by side on the showroom floor. There are significant differences. They did not just throw dual sport tires on the base scrambler.",
        "URL": "https://davemosstuning.com/scrambler-desert-sled-cafe-racer/",
        "Tags": [
            "Ducati",
            "settings",
            "standard",
            "street"
        ],
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "How-To: Radiator Guard Install",
        "Perex": " Radiators, while stout in many ways, are quite vulnerable in others. Rather than \"taking that change\" we decided to add a radiator guard to our GSX-R750 project bike.",
        "URL": "https://davemosstuning.com/how-to-radiator-guard-install/",
        "Tags": [
            "new parts"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Contact Patch: Pressure Pressure Pressure",
        "Perex": " More remote tuning with Dave Moss via text during each of the last 3 Trackdays at Utah Motorsports Campus (formerly Miller Motorsports Park) in the high desert. The thumbnail pic is a shredded front tire. The love affair between tire pressure and temperature is both beastly and beautiful (a little tire poetry there).",
        "URL": "https://davemosstuning.com/contact-patch-pressure-pressure-pressure/",
        "Tags": [
            "Contact Patch",
            "temperatures",
            "tires"
        ],
        "Categories": [
            "All",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Ask Dave: Geometry and Ability",
        "Perex": " When you first rode a bicycle, counter steering was immaterial because you weren't riding fast enough for the gyroscopic effect of the wheels to overcome standard steering. Ride fast enough and counter steering matters. Ride fast enough and geometry matters.",
        "URL": "https://davemosstuning.com/ask-dave-geometry-and-ability/",
        "Tags": [
            "Geometry",
            "street",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "How-To: Dry Cell Battery Install",
        "Perex": " 21st Century sportbike design is all about saving weight, especially up high in the chassis. So we installed a lightweight \"dry cell\" battery on our project GSX-R750.",
        "URL": "https://davemosstuning.com/how-to-dry-cell-battery-install/",
        "Tags": [
            "new parts"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "2 Clicks Out: Find Your Base Setup",
        "Perex": " Dave takes a stock bike (MV Agusta F3 800) with its showroom suspension settings and dials in a base suspension setup. Along the way he discovers the bike's strengths, and its challenges which will require upgrading. This basic process can be applied to any bike.",
        "URL": "https://davemosstuning.com/2-clicks-out-find-your-base-setup/",
        "Tags": [
            "forks",
            "Geometry",
            "settings",
            "Shocks",
            "Track"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Supersport Ergonomics",
        "Perex": " Dave always starts each suspension tune with a quick look at the ergonomics. Often some useful adjustments are warranted.",
        "URL": "https://davemosstuning.com/2-clicks-out-supersport-ergonomics/",
        "Tags": [
            "ergonomics",
            "sportbike"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "How-To: Chain \u0026 Sprocket Install part 2",
        "Perex": " Dave finishes up the 520 Conversion Kit chain and sprocket install on the project GSX-R750.  It should leap through the gears.",
        "URL": "https://davemosstuning.com/how-to-chain-sprocket-install-part-2/",
        "Tags": [
            "new parts"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Fork Service CBR1000RR 04-07",
        "Perex": " We do a complete fork service for the 2004 to 2007 Honda CBR1000RR Fireblade. Dave takes you through it step by step revealing a number of tips that make the task even smoother. Fear not, the Moss is with you. Always. James Toseland won the World Superbike Championship aboard the 2007 model. And Chris Vermeulen [...]",
        "URL": "https://davemosstuning.com/fork-service-cbr1000rr-04-07/",
        "Tags": [
            "forks",
            "Honda",
            "maintenance",
            "servicing"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Fork Oil Change CBR1000RR 04-07",
        "Perex": " We do a basic fork oil change for the 2004 to 2007 Honda CBR1000RR Fireblade. Fork oil thins as it ages reducing it's damping ability. Plus all the scraping of the spring on the inside of the fork tube creates metal shavings that can clog the valving. Fork oil should be changed at least every [...]",
        "URL": "https://davemosstuning.com/fork-oil-change-cbr1000rr-04-07/",
        "Tags": [
            "forks",
            "Honda",
            "servicing"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "2017 Triumph Thruxton R Showroom Evaluation",
        "Perex": " Dave evaluates the 2017 Triumph Thruxton, how it fits him, the state of the Ohlins suspension upgrades and how they could be adjusted for 3 different rider weight ranges.",
        "URL": "https://davemosstuning.com/2017-triumph-thruxton-r-showroom-evaluation/",
        "Tags": [
            "ergonomics",
            "settings",
            "street",
            "Suspension",
            "Triumph"
        ],
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "How-To: Chain \u0026 Sprocket Install part 1",
        "Perex": " Dave begins the install of a 520 Conversion Kit (chain and sprocket) on the project GSX-R750.  This is the most common sprocket based gear ratio change for most sport bikes to provide quicker acceleration off the line and through the gears.  It will even bump horsepower at the wheel by a pound.",
        "URL": "https://davemosstuning.com/how-to-chain-and-sprocket-install-part-1/",
        "Tags": [
            "new parts"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "April 1st, 2018",
        "Title": "Contact Patch: Hypersport Tire Comparison",
        "Perex": " Cycle Gear allowed us to play around in their tire vault recently. We found hypersport tires from each of the major brands and conducted a thorough investigation. Each is unique. Which one is right for you? Remember, in today's world there are no bad tires, only appropriate tires.",
        "URL": "https://davemosstuning.com/contact-patch-hypersport-tire-comparison/",
        "Tags": [
            "Contact Patch",
            "street",
            "tires"
        ],
        "Categories": [
            "All",
            "Reviews",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "Ask Dave: Considering a New Bike?",
        "Perex": " Dave's had a few over the years. Here he outlines his process when shopping for a new motorcycle.",
        "URL": "https://davemosstuning.com/ask-dave-considering-a-new-bike/",
        "Tags": [
            "ergonomics",
            "sag",
            "sportbike",
            "standard"
        ],
        "Categories": [
            "All",
            "Free",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "R6 Project: Installing Front Brake Lines",
        "Perex": " We install steel braided brake lines on our Yamaha R6 project bike. We show you how to remove your old brake lines and install a new set. This is one of the first and most common performance upgrades on a motorcycle.",
        "URL": "https://davemosstuning.com/r6-project-installing-front-brake-lines/",
        "Tags": [
            "new parts"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "2 Clicks Out: Crushing the ZX-6R",
        "Perex": " Dave enjoyed some time at Cycle Gear recently tuning street bikes. A couple of gentlemen arrived with a pair of new Kawasaki ZX-6R's, a 2016 with 5000 miles on it, and a 2017 with 800. Essentially identical bikes except for the VIN stickers. Were the setups identical?",
        "URL": "https://davemosstuning.com/2-clicks-out-crushing-the-zx-6r/",
        "Tags": [
            "Kawasaki",
            "settings",
            "sportbike",
            "street"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "Ask Dave: Fork Stiction",
        "Perex": " Stiction; it's a thing. For reals. Emphasis on the \"stuck.\" Like so much in life, it's solved with a bit of lube. But how, and where, and how much?",
        "URL": "https://davemosstuning.com/ask-dave-fork-stiction/",
        "Tags": [
            "forks",
            "maintenance"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "R6 Project: Front Brake Pad Install",
        "Perex": " We replace the front brake pads on our project Yamaha YZF-R6. In this video, we'll show you how to remove your old brake pads, and properly install new ones.",
        "URL": "https://davemosstuning.com/r6-project-front-brake-pad-install/",
        "Tags": [
            "Braking",
            "maintenance",
            "street"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "Contact Patch: Reading Tire Wear 1",
        "Perex": " Reading rear tire wear. Whats' the point? Every issue presented SLOWS you down, and, increases your risk of crashing. We combed through piles of used Bridgestone, Dunlop, Michelin and Pirelli rear tires to conduct some tire reading exercises. Hot tears, cold tears, geometry tears, wrong compound issues, and even some perfect wear. Remember, in today's [...]",
        "URL": "https://davemosstuning.com/contact-patch-reading-tire-wear-1/",
        "Tags": [
            "Contact Patch",
            "tires",
            "Track"
        ],
        "Categories": [
            "All",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "Ask Dave: Fork Air Spring",
        "Perex": " And they say nothing's free. Next time, just show them the air springs in your forks. But seriously, what does that air between the fork oil and the fork cap do anyway?",
        "URL": "https://davemosstuning.com/ask-dave-fork-air-spring/",
        "Tags": [
            "forks",
            "settings",
            "street",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "R6 Project: Front Brake Rotor Install",
        "Perex": " Because we crashed the bike, we're putting on some aftermarket brake rotors for good measure.",
        "URL": "https://davemosstuning.com/r6-project-front-brake-rotor-install/",
        "Tags": [
            "new parts"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "2 Clicks Out: Street Bikes at Cycle Gear",
        "Perex": " Recently Dave spent a morning tuning street bikes at Cycle Gear in Roseville, CA. In these settings he can take more time to explain what he's doing and why. Here we have a stock 600 and a 600 fitted with forks off a 1000.",
        "URL": "https://davemosstuning.com/2-clicks-out-street-bikes-at-cycle-gear/",
        "Tags": [
            "2 clicks out",
            "settings",
            "street"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "Ask Dave: Springs \u0026 Ability",
        "Perex": " Rider weight is the first aspect or stage determining the spring rates of your forks and shock. But it doesn't end there.",
        "URL": "https://davemosstuning.com/ask-dave-springs-ability/",
        "Tags": [
            "forks",
            "springs",
            "street",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "R6 Project: Installing Forks",
        "Perex": " After reinstalling the straightened triple clamps (yokes), Dave reinstalls the straightened forks onto our Yamaha R6 project bike.",
        "URL": "https://davemosstuning.com/r6-project-installing-forks/",
        "Tags": [
            "forks",
            "maintenance",
            "R6"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "2 Clicks Out: 2017 Multistrada",
        "Perex": " Our last Ducati Multistrada video really stirred the turds so we decided to dive deep into the 2017 model and see how all the electronic suspension works; unequivocally this time. Grab your favorite spoon and prepare to stir.",
        "URL": "https://davemosstuning.com/2-clicks-out-2017-multistrada/",
        "Tags": [
            "Ducati",
            "settings",
            "standard",
            "street"
        ],
        "Categories": [
            "All",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "How To: Changing Brake Pads",
        "Perex": " Dave installs new brake pads on 2 different types of brake calipers typical of most you'll see on today's bikes. Also cleaning the rotors, which generated much debate in a previous video recently.",
        "URL": "https://davemosstuning.com/how-to-changing-brake-pads/",
        "Tags": [
            "Braking",
            "maintenance",
            "servicing"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "R6 Project: Installing Triple Clamps (Yokes)",
        "Perex": " We just got our front end of our Yamaha R6 back from Gerry Piazza at GP Frame and Wheel. The forks and triple clamps were bent from a lowside crash. Gerry straightened them, now we just need to reassemble the front end, beginning with the triple clamps, or \"yokes\" as they are called in Dave's [...]",
        "URL": "https://davemosstuning.com/r6-project-installing-triple-clamps-yokes/",
        "Tags": [
            "maintenance",
            "servicing",
            "sportbike"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "Contact Patch: Rebound Tire Wear",
        "Perex": " 3 more bikes from our day at Cycle Gear. Two of them exhibit the exact same tire wear problem caused by rebound out of adjustment.",
        "URL": "https://davemosstuning.com/contact-patch-rebound-tire-wear-2/",
        "Tags": [
            "Contact Patch",
            "settings",
            "street",
            "tires"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "Ask Dave: Adverse Sprocket Wear",
        "Perex": " No sprocket lives forever, and rear sprockets have a much higher mortality rate than front sprockets. Some times they die prematurely.",
        "URL": "https://davemosstuning.com/ask-dave-adverse-sprocket-wear/",
        "Tags": [
            "Chain",
            "maintenance",
            "servicing"
        ],
        "Categories": [
            "All",
            "Free",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "R6 Project: Removing Forks \u0026 Triple Clamps (Yokes)",
        "Perex": " In the last episode of Performance Upgrades, Gerry Piazza of GP Frame and Wheel told us we had bent forks and triple clamps. Many times these expensive components can be straightened. In order to do so, the parts need to be removed from the bike and shipped away.",
        "URL": "https://davemosstuning.com/r6-project-removing-the-forks/",
        "Tags": [
            "maintenance",
            "servicing",
            "street"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "2 Clicks Out: Testing at Thunderhill",
        "Perex": " Dave conducts some preseason testing at Thunderhill in preparation for the AFM's opening round at Buttonwillow.",
        "URL": "https://davemosstuning.com/2-clicks-out-testing-at-thunderhill/",
        "Tags": [
            "racing",
            "settings",
            "Track",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "Ask Dave: Brake rotor stains",
        "Perex": " Back in the heyday of sportbiking ('00 thru '08) Galfer staff attended every AFM round in NorCal with a sand blasting apparatus to rectify the situation seen here.",
        "URL": "https://davemosstuning.com/ask-dave-brake-rotor-stains/",
        "Tags": [
            "Braking",
            "maintenance",
            "servicing"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "R6 Project: Frame Measuring",
        "Perex": " We take our project Yamaha YZF-R6 to Gerry Piazza of GP Frame and Wheel and ask him to give it to us straight. Gerry has the equipment to measure the crashed R6 and tell us where the bike is tweaked.",
        "URL": "https://davemosstuning.com/r6-project-frame-measuring/",
        "Tags": [
            "forks",
            "Geometry",
            "maintenance",
            "racing",
            "Shocks"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "Fork Service GSX-R600 \u0026 750 04-05",
        "Perex": " Dave guides us tenderly through a full fork service of the 2004 - 2005 GSX-R600 and GSX-R750. Bike development plateaued after the '08 recession, and bikes of this era remain extremely potent weapons of performance, comparatively speaking; vs something like a 1994 and 2004 model comparo. A comparo like that would be a violation... of... [...]",
        "URL": "https://davemosstuning.com/fork-service-gsx-r600-750-04-05/",
        "Tags": [
            "forks",
            "servicing",
            "Suzuki"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "Contact Patch: Tire Warmers",
        "Perex": " Street tires are, among other engineering traits, compounded to heat up quickly. Slicks, uh, no. For whatever reason it seems the stickier the tire, the longer it takes to heat up; even hyper sport tires vs touring tires. Hence the tire warmer. Dave explains their use and abuse.",
        "URL": "https://davemosstuning.com/contact-patch-tire-warmers/",
        "Tags": [
            "Contact Patch",
            "temperatures",
            "tire warmers",
            "tires"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "2009 R6 Project Bike Intro part 2",
        "Perex": " We continue with our new project bike, an 09 R6, by removing the rest of the bodywork to assess the crash damage. We encounter yet another type of fastener in the process.",
        "URL": "https://davemosstuning.com/2009-r6-project-bike-intro-part-2/",
        "Tags": [
            "maintenance",
            "R6",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "Contact Patch: Michelin Tire Family",
        "Perex": " Introducing the Michelin's, a nuclear family of sport tires. It's like the Simpson's, or The Housewives of New Michelinville. Except all members are honorable and adorable. Not an ugly one in the bunch.",
        "URL": "https://davemosstuning.com/contact-patch-michelin-tire-family/",
        "Tags": [
            "Contact Patch",
            "tires"
        ],
        "Categories": [
            "All",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "2009 R6 Project Bike Intro part 1",
        "Perex": " Welcome to our next project bike, a 2009 Yamaha R6. We crashed this bad boy ourselves and had to buy it, so now we're gonna fix it. First up, we strip the bodywork to assess the damage.",
        "URL": "https://davemosstuning.com/2009-r6-project-bike/",
        "Tags": [
            "maintenance",
            "street",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "2 Clicks Out: Motus",
        "Perex": " Their website says, \"Motus, the American sport-touring motorcycle.\" Rare beasts. But we encountered 2 in 2 days (one of them the Industry Award winner at the Quail show). Odds are weird that way. Maybe gold nuggets in the garden beds are next.",
        "URL": "https://davemosstuning.com/2-clicks-out-motus/",
        "Tags": [
            "2 clicks out",
            "settings",
            "standard"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "Ask Dave: Passenger Sag",
        "Perex": " The most significant factor in setting sag is rider weight. How does a passenger affect the equation? Dave knows.",
        "URL": "https://davemosstuning.com/ask-dave-passenger-sag/",
        "Tags": [
            "settings",
            "street",
            "Suspension"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "How-To: Installing Rearsets (foot rests)",
        "Perex": " We upgrade our project GSX-R750 with a set of Attack Performance aftermarket rearsets to gain more adjustability and performance when riding at the pointy end of the speed stick.",
        "URL": "https://davemosstuning.com/how-to-installing-rearsets-foot-rests/",
        "Tags": [
            "new parts"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "2008 Kawasaki ZX-10 Stock Suspension Evaluation",
        "Perex": " We evaluate the 2008 Kawasaki ZX-10R stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2008-kawasaki-zx-10-stock-suspension-evaluation/",
        "Tags": [
            "Kawasaki",
            "settings",
            "sportbike",
            "Suspension"
        ],
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "2 Clicks Out: Refining The Setup",
        "Perex": " A couple of buddies brought their KTM 690 Duke and Ducati Hypermotard 939 street bikes to a trackday. They spent most of the day refining their suspension with Dave as they went faster and put their bikes under increasing duress.",
        "URL": "https://davemosstuning.com/2-clicks-out-refining-the-setup/",
        "Tags": [
            "2 clicks out",
            "settings",
            "Track"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "How-To: Bleeding Brake Lines",
        "Perex": " Dave uses the Phoenix brake bleeding tool, but the general principles apply to the standard process as well.",
        "URL": "https://davemosstuning.com/how-to-bleeding-brake-lines/",
        "Tags": [
            "Braking",
            "maintenance",
            "servicing"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "How-To: Front Brake Line Install",
        "Perex": " Time now for upgraded front brake lines for the GSX-R750.  Steel braided lines require less squeeze on the lever; another way of saying it requires less brake pressure to slow the motorcycle.",
        "URL": "https://davemosstuning.com/how-to-front-brake-line-install/",
        "Tags": [
            "new parts"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "Ask Dave: Basic Sag",
        "Perex": " Demigods like Beaubier, Hayes, Spies, and Hayden (current and recent US demigods) do not bother with sag. The rest of us, for whom there is no data but raw tonnage, need some place to begin. Can't think outside the box without at least knowing where the box is.",
        "URL": "https://davemosstuning.com/ask-dave-basic-sag/",
        "Tags": [
            "sag",
            "Suspension"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "2007 Suzuki Bandit 1250 S Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Suzuki Bandit 1250 S stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2007-suzuki-bandit-1250-s-stock-suspension-evaluation/",
        "Tags": [
            "settings",
            "standard",
            "Suspension",
            "Suzuki"
        ],
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "2017 Liter Bike Comparison part 1",
        "Perex": " In part 1 we compare the Suzuki GSX-R1000, Honda CBR1000RR, and Kawasaki ZX-10R complete with Dave's recommended suspension adjustments.",
        "URL": "https://davemosstuning.com/2017-liter-bike-comparison-part-1/",
        "Tags": [
            "ergonomics",
            "sag",
            "settings",
            "sportbike"
        ],
        "Categories": [
            "All",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "How-To: Aftermarket Air Filter Install part 2",
        "Perex": " We finish up the air filter install on the project GSX-R750.  Full lung capacity achieved.",
        "URL": "https://davemosstuning.com/how-to-aftermarket-air-filter-install-part-2/",
        "Tags": [
            "new parts"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "How-To: Aftermarket Air Filter Install part 1",
        "Perex": " We install an aftermarket air filter on our project GSX-R750.  Nothing helps an engine inhale more freely than a performance oriented air filter.",
        "URL": "https://davemosstuning.com/how-to-aftermarket-air-filter-install/",
        "Tags": [
            "new parts"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "Ask Dave: Measuring Shock Sag",
        "Perex": " You'll often see Dave, after measuring fork sag, move to the back of the bike and measure shock sag, but from where does he measure.",
        "URL": "https://davemosstuning.com/ask-dave-measuring-shock-sag/",
        "Tags": [
            "sag",
            "Shocks"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "2008 Suzuki B-King Stock Suspension Evaluation",
        "Perex": " We evaluate the 2008 Suzuki B-King stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2008-suzuki-b-king-stock-suspension-evaluation/",
        "Tags": [
            "sag",
            "standard",
            "Suspension",
            "Suzuki"
        ],
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "2017 Liter Bike Comparison part 2",
        "Perex": " In part 2 we examine the 2017 Ducati 1299 Panigale S, the 2017 Yamaha R1, and the 2017 BMW S1000RR. We grouped these 3 together because they're unchanged for 2017. We've included a customer tune for the 1299 and the R1 as well (which made the video much longer). Unable to film an S1000RR customer [...]",
        "URL": "https://davemosstuning.com/2017-liter-bike-comparison-part-2/",
        "Tags": [
            "ergonomics",
            "settings",
            "sportbike",
            "Suspension"
        ],
        "Categories": [
            "All",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "How-To: Front Brake Pad Install",
        "Perex": " Next is a brake pad install for our 2007 Suzuki GSX-R750 project bike. Consumables for a sportbike are first gasoline, then tires, and then brake pads. Probably oil and brake pads fighting for 3rd place.",
        "URL": "https://davemosstuning.com/how-to-front-brake-pad-install/",
        "Tags": [
            "Braking",
            "maintenance",
            "new parts",
            "servicing"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 30th, 2018",
        "Title": "Ask Dave: Measuring Fork Sag",
        "Perex": " You see Dave measure fork sag in numerous videos. What's he doing besides mumbling and then stating whether the sag is correct or not?",
        "URL": "https://davemosstuning.com/ask-dave-measuring-fork-sag/",
        "Tags": [
            "forks",
            "sag"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 29th, 2018",
        "Title": "2008 Suzuki DR-Z400SM Stock Suspension Evaluation",
        "Perex": " We evaluate the 2008 Suzuki DR-Z400SM stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2008-suzuki-dr-z400sm-stock-suspension-evaluation/",
        "Tags": [
            "dual sport",
            "ergonomics",
            "settings",
            "Suspension"
        ],
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "FasterClass: Tire Pressure 101",
        "Perex": " There's an ancient Anglo Saxon proverb: Give a man tire pressures and he can ride for day. Teach a man how to set his tire pressures and he can ride for a lifetime. Listen to Dave now and hear him later.",
        "URL": "https://davemosstuning.com/classroom-tire-pressure-101/",
        "Tags": [
            "Fasterclass",
            "tires"
        ],
        "Categories": [
            "All",
            "How-To",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "How-To: Front Brake Rotor Install",
        "Perex": " Dave installs a set of aftermarket Galfer Wave rotors on the front of our GSX-R750 project bike.",
        "URL": "https://davemosstuning.com/how-to-front-brake-rotor-install/",
        "Tags": [
            "new parts"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "2 Clicks Out: Last Second Spring Change",
        "Perex": " Cliff, AFM racer 771, progressed beyond his fork spring's ability to support his increased pace. With just minutes before the first race of the day, Dave and a fellow tuner get to work on the fork spring swap. But it's never that simple... ever.",
        "URL": "https://davemosstuning.com/2-clicks-out-last-second-spring-change/",
        "Tags": [
            "2 clicks out",
            "forks",
            "springs"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "2008 Ducati Hypermotard 1100 Stock Suspension Evaluation",
        "Perex": " We evaluate the 2008 Ducati Hypermotard 1100 stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2008-ducati-hypermotard-1100-stock-suspension-evaluation/",
        "Tags": [
            "Ducati",
            "sag",
            "settings",
            "standard",
            "street"
        ],
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "2 Clicks Out: CBR1000RR Fest 08-16",
        "Perex": " Dave tunes 4 Honda CBR1000RR's from this 3rd generation of the Fireblade, 2008-2016. All street bikes with street tires. 2 with Ohlins suspension, 2 with standard suspension.",
        "URL": "https://davemosstuning.com/2-clicks-out-cbr1000rr-fest-08-16/",
        "Tags": [
            "Honda",
            "settings",
            "sportbike",
            "Track"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "How-To: Aftermarket Handle Bar Install part 2",
        "Perex": " In part 2 here we install the fully adjustable aftermarket clip-ons or handle bars on our project Suzuki GSX-R750.",
        "URL": "https://davemosstuning.com/how-to-aftermarket-handle-bar-install-part-2/",
        "Tags": [
            "new parts"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "How-To: Aftermarket Handle Bar Install part 1",
        "Perex": " We bin the bent stock handlebars from our project Suzuki GSX-R750 in preparation for a new set of fully adjustable aftermarket replacements.",
        "URL": "https://davemosstuning.com/how-to-aftermarket-handle-bar-install-part-1/",
        "Tags": [
            "new parts"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "Ask Dave: Tire Circumference",
        "Perex": " You would think all 180/55 rear tires are the same \"size\". They are in relationship to the wheel/rim. Otherwise, not so much; particularly with regard to circumference, which can significantly alter the handling characteristics of a motorcycle.",
        "URL": "https://davemosstuning.com/ask-dave-tire-circumference/",
        "Tags": [
            "street",
            "tires",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "2008 Ducati 848 Stock Suspension Evaluation",
        "Perex": " We evaluate the 2008 Ducati 848 stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2008-ducati-848-stock-suspension-evaluation/",
        "Tags": [
            "Ducati",
            "sag",
            "settings",
            "sportbike"
        ],
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "Contact Patch: Pirelli Track Tire Family",
        "Perex": " Time now for the Pirelli family of track tires and how to behave when you have them over for dinner.",
        "URL": "https://davemosstuning.com/contact-patch-pirelli-track-tire-family/",
        "Tags": [
            "Contact Patch",
            "street",
            "tires",
            "Track"
        ],
        "Categories": [
            "All",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "2 Clicks Out: Kawasaki ZX-14 Setup part3",
        "Perex": " We end with the rider's feedback regarding Dave's suspension setup of his Kawasaki ZX-14 daily ride.  Part 3 of a 3 part series",
        "URL": "https://davemosstuning.com/2-clicks-out-kawasaki-zx-14-setup-part3/",
        "Tags": [
            "2 clicks out",
            "Kawasaki",
            "sag",
            "settings",
            "sportbike"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "2 Clicks Out: Kawasaki ZX-14 Setup part2",
        "Perex": " We continue with Dave's street suspension setup of a Kawasaki ZX-14 for optimal handling. Part 2 of a 3 part series",
        "URL": "https://davemosstuning.com/2-clicks-out-kawasaki-zx-14-setup-part2/",
        "Tags": [
            "2 clicks out",
            "Kawasaki",
            "sag",
            "settings",
            "sportbike"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "2 Clicks Out: Kawasaki ZX-14 Setup part1",
        "Perex": " Dave helps a street rider dial in his Kawasaki ZX-14 suspension for optimal handling. Part 1 of a 3 part series",
        "URL": "https://davemosstuning.com/2-clicks-out-kawasaki-zx-14-setup-part1/",
        "Tags": [
            "2 clicks out",
            "Kawasaki",
            "sag",
            "settings",
            "sportbike"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "Ask Dave: Fork Oil Viscosity",
        "Perex": " Choices choices choices. 5 weight, 7.5 weight, 10 weight. Some is blue, some red, some green. Sheesh. Fork oil needs to be changed every 5000 miles. You can visibly see the difference in fork movement between identical ZX6R's in this video. https://youtu.be/bv0vCJd4TAg. One has 800 miles on it, the other 5000. How often do you [...]",
        "URL": "https://davemosstuning.com/ask-dave-fork-oil-viscosity/",
        "Tags": [
            "forks",
            "servicing"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "2008 Honda CBR600RR Stock Suspension Evaluation",
        "Perex": " We evaluate the 2008 Honda CBR600RR stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2008-honda-cbr600rr-stock-suspension-evaluation/",
        "Tags": [
            "ergonomics",
            "Honda",
            "sag",
            "settings"
        ],
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "2 Clicks Out: Fork Rebound Too Fast",
        "Perex": " When fork rebound is too fast (the forks extend too quickly after being compressed by a bump or hard braking) it causes serious performance issues. Yet it's relatively easy to check and correct.",
        "URL": "https://davemosstuning.com/2-clicks-out-fork-rebound-too-fast/",
        "Tags": [
            "forks",
            "settings",
            "sportbike"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "How-To: Steering Damper Install part 2",
        "Perex": " Project Gixxer continues as we bolt a GPR Steering Stabilizer on our GSX-R750 project bike. A Steering stabilizer greatly reduces what's known as headshake....AKA 'Tank Slapper'.",
        "URL": "https://davemosstuning.com/how-to-steering-damper-install-part-2/",
        "Tags": [
            "new parts"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "How-To: Steering Damper Install part 1",
        "Perex": " Project Gixxer, continues as we bolt a GPR Steering Stabilizer (damper) on our GSX-R750 project track bike. A Steering stabilizer greatly reduces what's known as headshake....AKA 'Tank Slapper'.",
        "URL": "https://davemosstuning.com/how-to-steering-damper-install-part-1/",
        "Tags": [
            "new parts"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "Ask Dave: Tire Sidewalls",
        "Perex": " Lots o'stuff embossed on the sidewall of a tire. Some tires have more embossing than others.",
        "URL": "https://davemosstuning.com/ask-dave-tire-sidewalls/",
        "Tags": [
            "tires",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "2008 Yamaha R6 Stock Suspension Evaluation",
        "Perex": " We evaluate the 2008 Yamaha R6 stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2008-yamaha-r6-stock-suspension-evaluation/",
        "Tags": [
            "ergonomics",
            "settings",
            "sportbike",
            "Suspension",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "FasterClass: Track Tire Pressure",
        "Perex": " Even a C group rider at the track can stress tires to points unachievable on the street. The sustained speeds and duress require an upgraded assessment of pressures and wear.",
        "URL": "https://davemosstuning.com/classroom-track-tire-pressure/",
        "Tags": [
            "Fasterclass",
            "tires",
            "Track"
        ],
        "Categories": [
            "All",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "How-To: Shock Settings",
        "Perex": " This is a basic introduction to motorcycle shock settings using a Yamaha R6 for the demo.  What are they, how to determine where they're set, and how to change them.",
        "URL": "https://davemosstuning.com/how-to-shock-settings/",
        "Tags": [
            "settings",
            "Shocks"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "How-To: Fork Settings",
        "Perex": " This is a basic introduction to motorcycle fork suspension settings using a Yamaha R6 for the demo.  What are they, how to determine where they're set, and how to change them.",
        "URL": "https://davemosstuning.com/how-to-fork-settings/",
        "Tags": [
            "forks",
            "settings"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "Ask Dave: Tire Pressure",
        "Perex": " Incorrect tire pressure is easily 50% of all tire woes. The faster you ride (street or track) the more important it becomes. Dave will explain.",
        "URL": "https://davemosstuning.com/ask-dave-tire-pressure/",
        "Tags": [
            "street",
            "tires",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "2008 Triumph Street Triple 675 Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Triumph Street Triple 675 stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2008-triumph-street-triple-675-stock-suspension-evaluation-2/",
        "Tags": [
            "ergonomics",
            "settings",
            "standard",
            "Suspension",
            "Triumph"
        ],
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "2 Clicks Out: 4 R1’s street track race \u0026 coach",
        "Perex": " Dave tunes 4 different 2015 Yamaha R1's, none with more than 1500 miles on it. Definite themes develop, both front and rear. The stock OEM tire is addressed (pressure pressure pressure). And the suspension settings off the showroom floor are all over the map.",
        "URL": "https://davemosstuning.com/2-clicks-out-4-r1s-street-track-race-coach/",
        "Tags": [
            "settings",
            "sportbike",
            "Track",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "How-To: Motorcycle Front Stands",
        "Perex": " There's an old homo sapiens saying, all good things come from god.  And motorcycle front stands are definitely good.  Nothing makes motorcycle maintenance more manageable than a good set of bike stands.  We use various Pit Bull stands on a GSX-R600 in this video, but there are many brands out there.",
        "URL": "https://davemosstuning.com/how-to-motorcycle-front-stands/",
        "Tags": [
            "new parts",
            "sportbike",
            "standard"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "How-To: Motorcycle Rear Stands",
        "Perex": " Nothing so sublime as using the correct tool for the task, and there is no better example of this principle than that of the motorcycle rear stand to support your bike during it's myriad maintenance evolutions.  We use Pit Bull stands on a GSX-R600 in this video, but there are many brands available.",
        "URL": "https://davemosstuning.com/how-to-motorcycle-rear-stands/",
        "Tags": [
            "new parts",
            "sportbike",
            "standard"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "Ask Dave: Tire Compounds",
        "Perex": " Next on the list of tire topics is tire compound, usually in the context of grip levels. Dave gets all zen and then composes a delightful lecture on the issue; namaste.",
        "URL": "https://davemosstuning.com/ask-dave-tire-compounds/",
        "Tags": [
            "tires",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "2007 Suzuki GSX-R750 Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Suzuki GSX-R750 stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2007-suzuki-gsx-r750-stock-suspension-evaluation/",
        "Tags": [
            "ergonomics",
            "settings",
            "Suspension",
            "Suzuki"
        ],
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "Fork Oil Change VFR800 98-01",
        "Perex": " Fresh fork oil can bring your 1998-2001 Honda VFR800 forks back to life. There's a quicky procedure and a proper procedure on these forks. Dave demo's the latter.",
        "URL": "https://davemosstuning.com/fork-oil-change-vfr800-98-01/",
        "Tags": [
            "forks",
            "Honda",
            "maintenance",
            "servicing"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 28th, 2018",
        "Title": "Fork Service VFR800 98-01",
        "Perex": " Dave tears down and rebuilds the 1998 to 2001 Honda VFR800 telescopic fork, including the fork seal. It's got a few idiosyncrasies which Dave handles with aplomb. He's known these forks since he was young man.",
        "URL": "https://davemosstuning.com/fork-service-vfr800-98-01/",
        "Tags": [
            "forks",
            "Honda",
            "maintenance",
            "servicing"
        ],
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "How-To: Setting Sag (using a GSX-R600) part 2",
        "Perex": " We've established our proper sag settings for a rider of average weight. Now we look at the rebound settings front and back, and we balance the bike for a comfortable, safe ride.",
        "URL": "https://davemosstuning.com/how-to-setting-sag-using-a-gsx-r600-part-2/",
        "Tags": [
            "sag",
            "sportbike",
            "Suzuki"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "How-To: Setting Sag (using a GSX-R600) part 1",
        "Perex": " Setting Sag on your sportbike is the first step to a safe and comfortable ride on your motorcycle. If the sag number is too small, the suspension settings or springs are too stiff for the riders weight.If the sag number is too large, the suspension settings or springs are too soft for the riders weight. [...]",
        "URL": "https://davemosstuning.com/how-to-setting-sag-using-a-gsx-r600-part-1/",
        "Tags": [
            "sag",
            "sportbike",
            "Suzuki"
        ],
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Ask Dave: Tire Carcass",
        "Perex": " Some tires have a stiff carcass, some a soft carcass, and some in the middle. All other aspects of tire construction and behavior descend from the foundation of a tire's carcass.",
        "URL": "https://davemosstuning.com/ask-dave-tire-carcass/",
        "Tags": [
            "street",
            "tires",
            "Track"
        ],
        "Categories": [
            "All",
            "Free",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2007 Suzuki GSXR 1000 Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Suzuki GSX-R1000 stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2007-suzuki-gsxr-1000-stock-suspension-evaluation/",
        "Tags": [
            "ergonomics",
            "settings",
            "sportbike",
            "Suspension",
            "Suzuki"
        ],
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2 Clicks Out: 5 new BMW S1000RR’s",
        "Perex": " Dave encounters a blitz of new BMW S1000RR's, two with less than 10 miles on them. Three sporting conventional suspension and two with inspector gadget, or the Dynamic Damping Control system (DDC or DDS for short, depending on ones anal retention). As usual the showroom settings are wanting, but the Bavarian bits themselves are sehr [...]",
        "URL": "https://davemosstuning.com/2-clicks-out-5-new-bmw-s1000rrs/",
        "Tags": [
            "BMW",
            "settings",
            "sportbike",
            "Track"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2 Clicks Out: Suspension and Winning",
        "Perex": " Years ago I (Dave Williams, editor) asked Mladin's mechanics what was the first thing they did to get more power out of a bike. They smiled politely and said power was the last thing they worried about. The first thing they addressed was suspension to make a bike go faster.",
        "URL": "https://davemosstuning.com/2-clicks-out-suspension-and-winning/",
        "Tags": [
            "2 clicks out",
            "racing",
            "Yamaha"
        ],
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2008 Ducati 1098S Stock Suspension Evaluation",
        "Perex": " We evaluate the 2008 Ducati 1098S stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2008-ducati-1098s-stock-suspension-evaluation/",
        "Tags": [
            "Ducati",
            "ergonomics",
            "sportbike",
            "Suspension"
        ],
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2 Clicks Out: Due Panigales, One Manual One DES",
        "Perex": " We encounter a pair of Ducati 1199 Panigales. The first is your standard Italian supermodel whose supple adjusters you caress manually to achieve breathless performance. The second is an S supermodel, a techno babe who requires electronic stimulation to satisfy your every performance desire.",
        "URL": "https://davemosstuning.com/2-clicks-out-due-panigales-one-manual-one-des/",
        "Tags": [
            "Ducati",
            "Panigale",
            "settings",
            "Track"
        ],
        "Categories": [
            "All",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2 Clicks Out – Graves Yamaha Fork Cartridge Change",
        "Perex": " In this episode of Two Clicks Out, Catalyst Reaction's Dave Moss watches the Graves Yamaha crew change out front fork cartridges on Daytona Sportbike rider Josh Herrin's YZF-R6 at the Yamaha Factory team test at Auto Club Speedway.",
        "URL": "https://davemosstuning.com/2-clicks-out-graves-yamaha-fork-cartridge-change/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2007 Suzuki GSX-R600 Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Suzuki GSX-R600 stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2007-suzuki-gsx-r600-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Ask Dave: Fork Oil Level",
        "Perex": " We all know there's oil in them there forks. So... how much and why? Answer me that, Mr. Dave Moss. G'ahead, try.",
        "URL": "https://davemosstuning.com/ask-dave-fork-oil-level/",
        "Tags": null,
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2 Clicks Out: ZX-10R’s, a 2015 \u0026 16",
        "Perex": " Dave tends twin Kawasaki Ninja ZX-10R's, born just 12 minutes... er... months apart; a 2015 and a 2016. Word has it Kawasaki took the geometry of Jonathan Rea's 2015 WSBK championship winning bike and manufactured it into the 2016 model, making it the most race spec bike ever offered to the common man.",
        "URL": "https://davemosstuning.com/2-clicks-out-zx-10rs-a-2015-16/",
        "Tags": null,
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2007 Kawasaki Ninja ZX-10R Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Kawasaki zx-10R stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2007-kawasaki-ninja-zx-10r-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Ask Dave: Fork Compression",
        "Perex": " Fork Compression damping completes the Trilogy or Triumvirate of adjusters that can be found on a set of motorcycle forks. Dave explains what it does, why we should care, etc etc.",
        "URL": "https://davemosstuning.com/ask-dave-fork-compression/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "600 Supersport Comparison 2017",
        "Perex": " Want to know why reviewers describe different handling characteristics for each bike in a shootout? We've created a unique version of a bike comparo, a version only Dave Moss is capable of producing. Each machine can deliver excellent performance. The question is which can be dialed in for you. In this video we tackle the [...]",
        "URL": "https://davemosstuning.com/600-supersport-comparison-2017/",
        "Tags": null,
        "Categories": [
            "All",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2007 Kawasaki Ninja 650R Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Kawasaki Ninja 650R stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2007-kawasaki-ninja-650r-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Ask Dave: Fork Rebound",
        "Perex": " So many questions, so little fork rebound. Dave explains the fork rebound essentials here. Thumbnail caption, \"Rebound, Grommet!\"",
        "URL": "https://davemosstuning.com/ask-dave-fork-rebound/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Contact Patch: Dunlop Track Tire Family",
        "Perex": " In this video we focus on the Dunlop family of Track Tires. It's small, like most families these days, but the members are all extreme athletes.",
        "URL": "https://davemosstuning.com/contact-patch-dunlop-track-tire-family/",
        "Tags": null,
        "Categories": [
            "All",
            "Reviews",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2007 Kawasaki Ninja ZX-14 Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Kawasaki ZX-14 stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2007-kawasaki-ninja-zx-14-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Ask Dave: Fork Preload",
        "Perex": " Dave gets hundreds of questions a week via email and social media, many on the topic of Fork Preload. Here's an all-in-one answer to fork preload FAQ's.",
        "URL": "https://davemosstuning.com/ask-dave-fork-preload/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2 Clicks Out: Old But Not Obsolete",
        "Perex": " Today's new bikes are less new than one might think. Take the new R6. Same frame and motor, just new electronics with R1 looking forks. It is surprising how capable preowned motorcycles can be. Here feature a Suzuki TL1000, a Ducati 900 SuperSport, and a Honda CB 750.",
        "URL": "https://davemosstuning.com/2-clicks-out-old-but-not-obsolete/",
        "Tags": null,
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2007 Kawasaki Ninja ZX-6R Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Kawasaki Ninja ZX-6R stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2007-kawasaki-ninja-zx-6r-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "AFM 450 Superbike Buttonwillow",
        "Perex": " The Superstock race went awry for Dave Moss. Can he beat his new rival Paul Johnson in the 450 SBK race, or will he find himself stuck on repeat.",
        "URL": "https://davemosstuning.com/afm-450-superbike-buttonwillow/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "AFM 450 Superstock Race Buttonwillow",
        "Perex": " The AFM 450 class grids get bigger each season. They're inline 4 cylinder 600's with one cylinder killed. Intermediate horsepower in a proper chassis. Dave Moss is the defending Champ... for now. These classes are attracting better talent every year.",
        "URL": "https://davemosstuning.com/afm-450-superstock-race-buttonwillow/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "AFM Formula 40 Lightweight Race Buttonwillow",
        "Perex": " Dave Moss races his beloved R4 in the AFM's Formula 40 light weight class. Definitely country for old (but not obsolete) men. Whose grey hair is the most distinguished? Watch and see.",
        "URL": "https://davemosstuning.com/afm-formula-40-lightweight-race-buttonwillow/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "300 Supersport Bike Comparison 2017",
        "Perex": " We've got the Honda CBR300R, Yamaha R3, Kawasaki Ninja 300, and KTM 390. Dave breaks them down and gives some suspension tuning suggestions for each.",
        "URL": "https://davemosstuning.com/300-supersport-bike-comparison-2017/",
        "Tags": null,
        "Categories": [
            "All",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2008 Triumph Street Triple 675 Stock Suspension Evaluation",
        "Perex": " We evaluate the 2008 Triump Street Triple 675 stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2008-triumph-street-triple-675-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2 Clicks Out: Post-Crash Bike Diagnosis",
        "Perex": " Dave crashed his beloved Yamaha R4 (an R6 with one cylinder disabled, a very popular class in AFM racing). The first step in the bike's rehab is having the frame measured. Is it still true, or is it bent?",
        "URL": "https://davemosstuning.com/2-clicks-out-post-crash-bike-diagnosis/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2 Clicks Out: After Setting Sag",
        "Perex": " In this video we start with the most basic of setups which includes setting sag. But that ball park setting isn't necessarily the end of the story. It's the beginning. The progression we chronicle is done with 4 Yamaha R6's to help maintain focus.",
        "URL": "https://davemosstuning.com/2-clicks-out-after-setting-sag/",
        "Tags": null,
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2007 Kawasaki Ninja 250R Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Kawasaki Ninja 250 stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2007-kawasaki-ninja-250r-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2 Clicks Out: CBR1000RR Quick Tune",
        "Perex": " People often ride their street bikes to a trackday event because Dave will be there. Such was the case with this Repsol replica 2011 Honda CBR1000RR. He did not participate in the trackday at all. Just rode to the event with some friends to get their street bikes set up by Dave.",
        "URL": "https://davemosstuning.com/2-clicks-out-cbr1000rr-quick-tune/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Contact Patch: Braking Lines",
        "Perex": " The front tire must strike a balance between flexing enough to create a contact patch and maintaining enough shape to steer properly. Braking lines are a data point visible on the tire indicating whether you're in the OPZ, the optimum grip zone (OPZ is not a thing, really. I, Dave the editor, just made it [...]",
        "URL": "https://davemosstuning.com/contact-patch-braking-lines/",
        "Tags": null,
        "Categories": [
            "All",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2007 Kawasaki Ninja 500R Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Kawasaki Ninja 500R stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2007-kawasaki-ninja-500r-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2 Clicks Out: Nullified Hydraulics",
        "Perex": " Here Dave assists a GSX-R1000 with fork issues. A turn here, a turn there and, poof, he almost gets invited to Christmas dinner.",
        "URL": "https://davemosstuning.com/2-clicks-out-nullified-hydraulics/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "650 / 700 Comparison 2017",
        "Perex": " In this video Dave reviews and compares the Suzuki SV650, Kawasaki Z650, Honda CBR650F, Honda NC700X, Yamaha FZ07, and Ducati 797. He examines how he fits each bike, their suspension componentry, and how he would setup each bike for riders in 3 weight categories.",
        "URL": "https://davemosstuning.com/650-700-comparison-2017/",
        "Tags": null,
        "Categories": [
            "All",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2007 Kawasaki ZZR600 Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Kawasaki ZZR600 stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2007-kawasaki-zzr600-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2 Clicks Out: High Side Protocols",
        "Perex": " A gentleman came to Dave after his Ducati 999 stepped out disconcertingly in a very high speed sweeper at Buttonwillow Raceway. Come to find out he'd high sided his pretty-in-pink 999 the trackday before. Dave initiated a set of crash protocols on the wounded Duc before diagnosing and treating anything else.",
        "URL": "https://davemosstuning.com/2-clicks-out-high-side-protocols/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2 Clicks Out: Tire Circumference Cold Tear",
        "Perex": " You'll hear Dave talk about the importance of paying close attention to the circumference of your rear tire, that changing brands and/or models within a brand can significantly alter your bike's handling because of it. Behold, exhibits A and B of said significance.",
        "URL": "https://davemosstuning.com/2-clicks-out-tire-circumference-cold-tear/",
        "Tags": null,
        "Categories": [
            "All",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2008 Honda CBR1000RR Stock Suspension Evaluation",
        "Perex": " We evaluate the 2008 Honda CBR1000RR stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2008-honda-cbr1000rr-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Suspension Myth #3: Riding Schools",
        "Perex": " Suspension Myth 3 rolls the first 2 Myths into the context of riding schools. Everyone should attend a riding school at a track. The benefits are enormous, even for street riders. Myth 3 deals with the impact your new found riding skills have on your bike's ability to cope with said new skills.",
        "URL": "https://davemosstuning.com/suspension-myth-3-riding-schools/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Fork Service Yamaha R6 08-15",
        "Perex": " Yamaha R6 forks remained unchanged from 2008 to 2015; quite a run. All good things are persistent. Dave performs a detailed fork service on a set to keep them in tip top shape",
        "URL": "https://davemosstuning.com/fork-service-yamaha-r6-08-15/",
        "Tags": null,
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Suspension Myth #2",
        "Perex": " Several suspension myths persist even in this age of total enlightenment. We explode these myths right here, right now, by addressing Suspension Myth #2, the diabolical child of Myth 1.",
        "URL": "https://davemosstuning.com/suspension-myth-2/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Fork Oil Change Yamaha R6 Fork 08-15",
        "Perex": " The Yamaha R6 forks are the same from 2008 to 2015. A long run, that. Dave performs a step by step oil change on a set to keep them fresh and vibrant.",
        "URL": "https://davemosstuning.com/fork-oil-change-yamaha-r6-fork-08-15/",
        "Tags": null,
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2007 Suzuki V-Strom 1000 Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Suzuki V-Strom 1000 stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2007-suzuki-v-strom-1000-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Suspension Myth #1",
        "Perex": " Several suspension myths persist even in this age of total enlightenment. We explode these myths right here, right now, beginning with the progenitor of all myths, Myth #1.",
        "URL": "https://davemosstuning.com/suspension-myth-1/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2 Clicks Out: AFM Racer Changes Tire Brands",
        "Perex": " AFM racer Elliot, #799, is changing tire brands, from Pirelli to Michelin, and switching to slicks from DOT's. This consequently includes a change in the size of the rear tire which, of course, changes the bike's geometry. Follow the progress of Dave and Elliot's suspension tune and tire wear issues throughout the Saturday practice sessions [...]",
        "URL": "https://davemosstuning.com/2-clicks-out-afm-racer-changes-tire-brands/",
        "Tags": null,
        "Categories": [
            "All",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2007 Suzuki V-Strom 650 Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Suzuki 650 V-Strom's stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2007-suzuki-v-strom-650-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2 Clicks Out: 2012 ZX6R Quick Setup",
        "Perex": " A trackday enthusiast has had Dave setup his 2012 Kawasaki Ninja ZX6R in the past. Now he's riding faster and that setup can't cope with his new pace. Going faster puts greater demands on the bike, and setup must be adjusted to suit.",
        "URL": "https://davemosstuning.com/2-clicks-out-2012-zx6r-quick-setup/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2 Clicks Out: Racing the Ninja 300",
        "Perex": " Ashlee races the Kawasaki Ninja 300. Unlike the ZX6 and ZX10 these bikes don't come with fully adjustable suspension. On the flip side, they're more affordable, AND can be upgraded as the rider's ability and/or bank account improves. It's about the journey, etc etc etc.",
        "URL": "https://davemosstuning.com/2-clicks-out-racing-the-ninja-300/",
        "Tags": null,
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Honda 2007 VFR Interceptor Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Honda VFR's stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/honda-2007-vfr-interceptor-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Performance Upgrades: HM Quickshifter",
        "Perex": " Dave installed an HM quickshifter on his Yamaha R4, then took it to the track for a test. They say it's worth a tenth of a second per shift. That's roughly a second a lap on the average track. Hello factory ride.",
        "URL": "https://davemosstuning.com/performance-upgrades-hm-quickshifter/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Fork Service SV650 \u0026 650S 99-12",
        "Perex": " This fork service video covers both the standard Suzuki SV650 and the Suzuki SV650S which has a preload adjuster. Otherwise the forks are identical, and Dave demos everything you need to know. And, yes, the forks never changed from '99 to '12. If it ain't broke...",
        "URL": "https://davemosstuning.com/fork-service-sv650-650s-99-12/",
        "Tags": null,
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Fork Oil Change SV650 \u0026 650S 99-12",
        "Perex": " This fork oil change video covers both the standard Suzuki SV650 and the Suzuki SV650S which has a preload adjuster. Otherwise the forks are identical, and Dave demos everything you need to know. And, yes, the forks never changed from '99 to '12. If it ain't broke...",
        "URL": "https://davemosstuning.com/fork-oil-change-sv650-650s-99-12/",
        "Tags": null,
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Martin Cardenas: Huevos, My Friend",
        "Perex": " Martin Cardenas entered the VIR round of the 2009 Daytona Sportbike championship tied for first place. A crash in the opening practice session made his championship pursuit even more challenging.",
        "URL": "https://davemosstuning.com/martin-cardenas-huevos-my-friend/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Yamaha 2007 YZF600R Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Yamaha YZF600R's stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/yamaha-2007-yzf600r-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "How To: Ninja 250 Chain Adjustment",
        "Perex": " A Kawasaki Ninja 250 arrived for a school day at the track with a slack chain. Can't be too tight, can't be too loose. There's truth in that Goldilocks fairy tale, it needs to be just right. That's why god gave us adjusters. These on the Ninja 250 are typical for a modestly powered bike. [...]",
        "URL": "https://davemosstuning.com/how-to-ninja-250-chain-adjustment/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2 Clicks Out: Women Do Ride Fast Bikes… Fast",
        "Perex": " Few men take up ballet. Few women take up sportbiking. These 3 women at the track ride serious machinery, a 2015 Triumph 675R, a 2009 Kawasaki Ninja ZX-10R, and a 20?? Ducati 999.",
        "URL": "https://davemosstuning.com/2-clicks-out-women-do-ride-fast-bikes-fast/",
        "Tags": null,
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "GSX-R750 Project Rescue",
        "Perex": " We take on a Suzuki GSX-R750 in serious need of some TLC. She shows great potential, but she was in pretty rough shape when we took possession. Blown head gasket, bent sub-frame, trashed body work, bent handlebars. The list goes on. We are going to throw some choice parts at this thing and get it [...]",
        "URL": "https://davemosstuning.com/gsx-r750-project-rescue/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2 Clicks Out: Multistrada 1200 Pikes Peak",
        "Perex": " Advanced citizens will remember the days when power windows and locks on new cars were luxury upgrades, not standard fare. Back then we often passed on those gadgets because, \"it's just something else that'll break.\" Electronically controlled suspension adjusters are in that phase of development. Sometimes the cutting edge cuts out.",
        "URL": "https://davemosstuning.com/2-clicks-out-multistrada-1200-pikes-peak/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2 Clicks Out: Naked On Track, S1000R GSX-S1000 \u0026 FZ-09",
        "Perex": " It's almost like a swimsuit issue. These naked bikes come fully equipped with top shelf suspension bits begging to be tweaked. There's no performance lost with the loss of fairings. No shirt, no pants, plenty of service.",
        "URL": "https://davemosstuning.com/2-clicks-out-naked-on-track-s1000r-gsx-s1000-fz-09/",
        "Tags": null,
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Triumph Thruxton 2007 Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Triumph Thruxton's stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/triumph-thruxton-2007-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "How To: Hand Control Ergonomics",
        "Perex": " Dave helps a rider adjust her handle bars and levers to the get maximum control over her bike.",
        "URL": "https://davemosstuning.com/how-to-hand-control-ergonomics/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2 Clicks Out: Lowered 600’s, a ZX-6R \u0026 R6",
        "Perex": " We encountered a couple of lowered 600's, a Kawasaki Ninja ZX-6R at a trackday and a Yamaha R6 at a riding school.",
        "URL": "https://davemosstuning.com/2-clicks-out-lowered-600s-a-zx-6r-r6/",
        "Tags": null,
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Triumph Tiger 2007 Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Triumph Tiger's stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/triumph-tiger-2007-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2 Clicks Out: Tuning a new racer",
        "Perex": " After intense deliberation and soul searching consideration, Fabian Fernandez decided to go racing. He consulted with Dave in the preseason regarding suspension components for his Yamaha R6, and then worked with Dave during his inaugural weekend dialing it in.",
        "URL": "https://davemosstuning.com/2-clicks-out-tuning-a-new-racer/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2 Clicks Out: The Venerable CBR600 F4i",
        "Perex": " The Honda CBR600F4i remains a potent weapon even by today's standards. The \"i\" of course means fuel injection rather than carbureted, and the telescopic forks are fully adjustable. The 2 we encountered are both street bikes come to the track, one at a riding school, and the other for a rider's first trackday.",
        "URL": "https://davemosstuning.com/2-clicks-out-the-venerable-cbr600-f4i/",
        "Tags": null,
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "How-To: GSX-R750 Front Brake Line Upgrade",
        "Perex": " We install front brake lines on our project GSX-R750. Steel braided lines provide improved braking performance over the stockers.",
        "URL": "https://davemosstuning.com/how-to-gsx-r750-front-brake-line-upgrade/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Aprilia Tuono 1000 R Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Aprilia Tuono 1000R stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/aprilia-tuono-1000-r-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2 Clicks Out: Dunlop cold tear \u0026 hydraulic locking an RSV1000R",
        "Perex": " Wheelie issues, a cold tear on the Dunlops, and then hydraulically locking the forks haunt a 2007 Aprilia RSV 1000 R at Sears Point in Sonoma, CA. Even us mortals can override the equipment. It's amazing what an old school zip tie data logger can reveal. We went to the moon using a slide rule, [...]",
        "URL": "https://davemosstuning.com/2-clicks-out-dunlop-cold-tear-hydraulic-locking-an-rsv1000r/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "2 Clicks Out: A boy and his ZX6R",
        "Perex": " A young man brings his new (to him) Kawasaki ZX6R to the track. Can suspension constructed to support the ever expanding American girth be made to work for a boy weighing all of 120 lbs?",
        "URL": "https://davemosstuning.com/2-clicks-out-a-boy-and-his-zx6r/",
        "Tags": null,
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Aprilia SXV 4.5 Supermotard Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Aprilia SXV 450 stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/aprilia-sxv-4-5-supermotard-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "How To: Caring for an Ohlins TTX shock",
        "Perex": " The more serious one becomes about a sport, the more serious becomes their equipment. A natural consequence is the need to take more seriously the care and maintenance of said equipment. An Ohlins TTX Shock is not exempt.",
        "URL": "https://davemosstuning.com/how-to-caring-for-an-ohlins-ttx-shock/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Fork Service Speed Triple 1050 05-09",
        "Perex": " Dave demonstrates (and of course performs, before your very eyes and ears) a full fork service on the 2005-2009 Triumph Speed Triple 1050 forks.",
        "URL": "https://davemosstuning.com/fork-service-speed-triple-1050-05-09/",
        "Tags": null,
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "Fork Oil Change Speed Triple 1050 05-09",
        "Perex": " Dave explains and demonstrates a fork oil change on the 2005-2009 Triumph Speed Triple 1050 forks. Like other things of theirs, some of it is on the wrong side.",
        "URL": "https://davemosstuning.com/fork-oil-change-speed-triple-1050-05-09/",
        "Tags": null,
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 27th, 2018",
        "Title": "How-To: Foot Control Adjustability",
        "Perex": " Every bike's controls come with some sort of adjustability. Dave Moss shows us the availably features on a Suzuki GSX-R600.",
        "URL": "https://davemosstuning.com/how-to-foot-control-adjustability/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "Suzuki 2007 SV1000S Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Suzuki SV1000S stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/suzuki-2007-sv1000s-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "How to: Single sided swingarm chain adjustment",
        "Perex": " Adjusting the chain on a single sided swingarm, in this case a 2007 Ducati Monster S2R, is both simpler and \"complexer\" than adjusting the chain on a standard bike. Also in this video Dave sets the cold tire pressure on the bike's Dunlop Q3's for a trackday. Very secret stuff, that. Some say all good [...]",
        "URL": "https://davemosstuning.com/how-to-single-sided-swingarm-chain-adjustment/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "2 Clicks Out: Triumph 675 \u0026 675R",
        "Perex": " We encountered one of each earlier this year. Excellent bikes, race by the likes of Chaz Davies in World Supersport, and even our own Mr. Dave Moss in the AFM. You just have to get over an intense desire to ride on the wrong side of the road.",
        "URL": "https://davemosstuning.com/2-clicks-out-triumph-675-675r/",
        "Tags": null,
        "Categories": [
            "All",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "Suzuki 2007 SV650S Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Suzuki SV650S stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/suzuki-2007-sv650s-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "Performance Upgrades: JRI Shock testing",
        "Perex": " Dave puts the new JRI after market shock through its paces on the MV Agusta F3 800. Pre installed settings suitable for the C group must, of course, be changed to cope with the A group pace. Fork settings compatible with the stock shock become inadequate as the JRI shock begins to enhance the F3 [...]",
        "URL": "https://davemosstuning.com/performance-upgrades-jri-shock-testing/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Testing Program",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "2 Clicks Out: Aprilia RS250",
        "Perex": " Here's a rarified ride, an Aprilia RS250 track bike; the closest thing to a real GP bike the masses can afford. Forget \"location location location.\" It's chassis chassis chassis in our world. When comparing World Supersport lap times with Moto 2 lap times (running Honda CBR 600 blowup proof motors) we can empirically conclude a [...]",
        "URL": "https://davemosstuning.com/2-clicks-out-aprilia-rs250/",
        "Tags": null,
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "Ducati 2007 Sport 1000 S Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Ducati Sport 1000 S stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/ducati-2007-sport-1000-s-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "Contact Patch: Ninja 300 shock causes tire wear",
        "Perex": " Laura's Kawasaki Ninja 300 is fitted with it's stock OEM shock. With the adjusters max'd out it can no longer cope with her increased pace. This creates handling issues on corner entry which are clearly manifest in a particular tire wear pattern. The temporary solution is a riding style change.",
        "URL": "https://davemosstuning.com/contact-patch-ninja-300-shock-causes-tire-wear/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "2 Clicks Out: Duke Motard Dorsoduro",
        "Perex": " Dave tunes 3 supermoto style bikes, a Ducati Hypermotard 796, an Aprilia Dorsoduro, and a KTM 690 Duke; all kindred spirits for sure.",
        "URL": "https://davemosstuning.com/2-clicks-out-duke-motard-dorsoduro/",
        "Tags": null,
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "Ducati 2007 Sport 1000 Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Ducati Sport 1000's stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/ducati-2007-sport-1000-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "2 Clicks Out: Finding Fork Bottom Out",
        "Perex": " Regardless of the bike you ride or where you ride it, knowing where your forks bottom out and whether you're close to doing so is vital to remaining helmet side up. It's a simple thing to determine, measure, and monitor especially if you are trying to set sag to know where you are for your [...]",
        "URL": "https://davemosstuning.com/2-clicks-out-finding-fork-bottom-out-2/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "2 Clicks Out: The 2007 Yamaha R1",
        "Perex": " Troy Corser and Eric Bostrom are a couple of the greats who raced the 2007 Yamaha R1. Noriyuki Haga won 6 World Superbike races with it. We encountered 2 of them (the bike, not the greats) at the same trackday. What are the odds? 9 years later and these bikes remain excellent rides.",
        "URL": "https://davemosstuning.com/2-clicks-out-the-2007-yamaha-r1/",
        "Tags": null,
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "Triumph 2007 Sprint ST Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Triumph Sprint ST's stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/triumph-2007-sprint-st-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "2 Clicks Out: Clicks, Turns, \u0026 Faces",
        "Perex": " You may hear Dave muttering numbers to himself from time to time while using suspension related tools. In this video he explains what he's counting when dialing in the appropriate setting of the various adjusters involved.",
        "URL": "https://davemosstuning.com/2-clicks-out-clicks-turns-faces/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "2 Clicks Out: Spies and Hayes era R1’s",
        "Perex": " Dave tunes a pair of championship crushing Yamaha R1's, the 2009 and 2012 models. Rookie Ben Spies razed World Superbike wielding the '09 model, taking 11 of 14 Superpoles and winning Half the races. Josh Hayes laid AMA Superbike to waste aboard the '12 model, winning a record16 of 20 races (next is Mladin with [...]",
        "URL": "https://davemosstuning.com/2-clicks-out-spies-and-hayes-era-r1s/",
        "Tags": null,
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "Ducati 2007 SS800 Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Ducati SS800's stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/ducati-2007-ss800-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "Performance Upgrades: JRI Shock Install",
        "Perex": " We install a brand new JRI shock in the MV Agusta F3 800, a prototype for this bike. We rolled up to Button Willow raceway late one afternoon, put the bike on the stands, broke the seals on the box containing the feather weight shock, and went to work. Wrench on.",
        "URL": "https://davemosstuning.com/performance-upgrades-jri-shock-install/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Reviews",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "How-To: Hand Control Adjustability",
        "Perex": " Every motorcycle has adjustable hand controls yet few riders take the time to explore these options. Dave Moss walks us through the adjustable features on a Suzuki GSX-R600.",
        "URL": "https://davemosstuning.com/how-to-hand-control-adjustability/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "Fork Service CBR600RR 03-04",
        "Perex": " Both the 2003 and 2004 Honda CBR600RR had telescopic forks. Dave performs a complete fork service, step by step. Chris Vermeulen won the World Supersport Championship with Ten Kate Honda on the 2003 model, and Karl Muggeridge the following year on the 2004 model, having replaced the departed Vermeulen. Fine machinery even by today's standards. [...]",
        "URL": "https://davemosstuning.com/fork-service-cbr600rr-03-04/",
        "Tags": null,
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "Fork Oil Change CBR600RR 03-04",
        "Perex": " Dave performs a fork oil change on the 2003 / 2004 Honda CBR600RR telescopic fork. Chris Vermeulen won the World Supersport Championship with Ten Kate Honda on the 2003 model, and Karl Muggeridge the following year on the 2004 model. Fine machinery even by today's standards.",
        "URL": "https://davemosstuning.com/fork-oil-change-cbr600rr-03-04/",
        "Tags": null,
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "2 Clicks Out: SV650 \u0026 GSXR600 racers",
        "Perex": " The Suzuki SV650 and GSX-R600 in this video are proof positive bikes built since the turn of the century have remarkable performance longevity. The difference between a 2005 sportbike and a 2015 is infinitesimal compared to that between a 1995 and 2005 model. Did they even have tubeless tires in 1995?",
        "URL": "https://davemosstuning.com/2-clicks-out-sv650-gsxr600-racers/",
        "Tags": null,
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "2007 Aprilia RSV 1000R Stock Suspension Evaluation",
        "Perex": " We evaluate the 2007 Aprilia RSV 1000R's stock suspension settings off the showroom floor with 3 different people representing 3 rider weight ranges.  Just who is the suspension designed for out of the box?",
        "URL": "https://davemosstuning.com/2007-aprilia-rsv-1000r-stock-suspension-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "Contact Patch: Michelin Power Supersport Evo",
        "Perex": " Dave encounters a set of Michelin's new Power Supersport Evo's mounted on a 2013 Honda CBR600RR. Remember, there are no bad tires, only misunderstood tires. The relationship began with a basic cold pressure first date and escalated from there.",
        "URL": "https://davemosstuning.com/contact-patch-michelin-power-supersport-evo/",
        "Tags": null,
        "Categories": [
            "All",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "2 Clicks Out: ZX-6R Racer part 2",
        "Perex": " Part 2 of the ZX-6R initial analysis. As Dave takes a closer look, there are some other points of interest identified requiring further attention.",
        "URL": "https://davemosstuning.com/2-clicks-out-zx-6r-racer-part-2/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "2 Clicks Out: ZX-6R Racer part 1",
        "Perex": " The first in a series of videos from WRA's Ls Vegas round of a Kawasaki ZX6 that comes under the canopy for an assessment. As always, it is all about preparation and our rider Kristi wants to make sure her race bike is ready for the track.",
        "URL": "https://davemosstuning.com/2-clicks-out-zx-6r-racer-part-1/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "Contact Patch: Tire Temp = Tire Pressure",
        "Perex": " The correct temperature of your motorcycle tires will provide you with the best grip and the best wear. Consequently you'll get the most life out of them as well. Tire pressure determines tire temperature at speed. What's yours?",
        "URL": "https://davemosstuning.com/contact-patch-tire-temp-tire-pressure/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 26th, 2018",
        "Title": "2 Clicks Out: an MV Agusta F4 \u0026 Brutale",
        "Perex": " MV Agustas are not a dime a dozen. Dave sets up an F4 for a track day regular and a Brutale for a school rider.",
        "URL": "https://davemosstuning.com/2-clicks-out-an-mv-agusta-f4-brutale/",
        "Tags": null,
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 21st, 2018",
        "Title": "Contact Patch: Ninja 300 stresses the front tire",
        "Perex": " Laura rides a Kawasaki Ninja 300. After a basic suspension tune she went faster, of course. Her increased pace created an issue with her front tire. Such are the trials of going faster. Just another first world problem.",
        "URL": "https://davemosstuning.com/contact-patch-ninja-300-stresses-the-front-tire/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 21st, 2018",
        "Title": "2 Clicks Out: When Geometry Matters",
        "Perex": " In this episode Dave encounters some next level issues, one involving shock compression and the other involving front end geometry. Sometimes a bike's geometry out of the box is spot on, and sometimes not so much. The patients here just happen to be Kawasaki Ninja's, a ZX-10R and a ZX-6R, but other brands have produced [...]",
        "URL": "https://davemosstuning.com/2-clicks-out-when-geometry-matters/",
        "Tags": null,
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 21st, 2018",
        "Title": "2 Clicks Out: Finding Fork Bottom Out",
        "Perex": " Regardless of the bike you ride or where you ride it, knowing where your forks bottom out and whether you're close to doing so is vital to remaining helmet side up. It's a simple thing to determine, measure, and monitor especially if you are trying to set sag to know where you are for your [...]",
        "URL": "https://davemosstuning.com/2-clicks-out-finding-fork-bottom-out/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 21st, 2018",
        "Title": "2 Clicks Out: Michelin Cold Tear \u0026 R6 Geometry",
        "Perex": " Another R6 enters Dave's clinica mobile for treatment. While the most common 600 raced in America, each seems uniquely altered in search of some outside-the-box magic. Or maybe it's just random wrenching. This one is also shod with Michelin slicks suffering a cold tear, and stubbornly so. \"Heal me...\" it moaned softly.",
        "URL": "https://davemosstuning.com/2-clicks-out-michelin-cold-tear-r6-geometry/",
        "Tags": null,
        "Categories": [
            "All",
            "How-To",
            "Suspension",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 20th, 2018",
        "Title": "Safety Wire Pliers",
        "Perex": " Veteran mechanic, Greg Bunting, at Mach 1 Motorsports in Vallejo, CA loaned us his pair of safety wire pliers for one of our video projects. The design of the tool hasn't changed in 50 years.",
        "URL": "https://davemosstuning.com/safety-wire-pliers/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "How-To",
            "Video"
        ]
    },
    {
        "Date": "March 16th, 2018",
        "Title": "2 Clicks Out: Ducati Monster 696 geometry",
        "Perex": " Chika rides a Ducati Monster 696. Dave makes an adjustment to the shock and then a geometry change in the front to correct a steering issue inherent off the showroom floor. In today's world there really are no bad motorcycles. They can all be adjusted to handle beautifully.",
        "URL": "https://davemosstuning.com/2-clicks-out-ducati-monster-696-geometry/",
        "Tags": null,
        "Categories": [
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "March 15th, 2018",
        "Title": "Contact Patch: Reading Tire Wear 2",
        "Perex": " Front tires this time. Again, there are no bad tires in today's sportbike world, only misunderstood pressures and suspension settings. No tire was born a bad tire. Every issue described here SLOWS you down and increases your risk of crashing; street or track.",
        "URL": "https://davemosstuning.com/13174-2/",
        "Tags": null,
        "Categories": [
            "All",
            "How-To",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 13th, 2018",
        "Title": "Qatar, Dunlop 6757 compound",
        "Perex": "  Dunlop 195 rear, date code 5011, compound 6757. 2012 BMW S1000RR NOTE: The massive tire tearing issues at Losail were consistent with almost all riders irrespective of pace or make of bike. The first thing to fix is carcass temps, so we started with 1.5bar, then as the track and day warmed up depending [...]",
        "URL": "https://davemosstuning.com/qatar-dunlop-6757-compound/",
        "Tags": null,
        "Categories": [
            "All",
            "Article",
            "Tires"
        ]
    },
    {
        "Date": "March 13th, 2018",
        "Title": "FasterClass: Manufacturer Tire Pressures",
        "Perex": " Manufacturer Recommended Tire Pressures. Dave spends hours and hours every week answering questions about this (via email and social media). Here's a primer.",
        "URL": "https://davemosstuning.com/classroom-manufacturer-tire-pressures/",
        "Tags": null,
        "Categories": [
            "All",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "March 12th, 2018",
        "Title": "2 Clicks Out: Multistrada 1200 Off The Showroom Floor",
        "Perex": "            Premium Video Trailer.  To watch the full video please Subscribe or Login.        Want to see the full video?    Get Dave Moss Total Access    Already a Member?     ",
        "URL": "https://davemosstuning.com/2-clicks-out-mu-multistrada-1200-off-the-showroom-floor-3/",
        "Tags": [
            "Video"
        ],
        "Categories": [
            "All",
            "Reviews",
            "Video"
        ]
    },
    {
        "Date": "January 4th, 2018",
        "Title": "Dunlop Q3 track day tire initial evaluation",
        "Perex": " Keigwin’s at The Track Novice School   Laguna Seca August 5th \u0026 6th   Ambient high 65F Having seen only one set of Dunlop Q3 tires so far this year prior to the School that were on a Triumph Speed Triple, I didn’t have much repetitive data to draw from other than the carcass was completely [...]",
        "URL": "https://davemosstuning.com/dunlop-q3-track-day-tire-initial-evaluation/",
        "Tags": null,
        "Categories": [
            "All",
            "Article",
            "Tires"
        ]
    },
    {
        "Date": "January 4th, 2018",
        "Title": "Ducati Panigale standard",
        "Perex": " Panigale standard   Features and settings Fork caps: –       are “extended” to allow additional fork positioning over stock to allow greater rake and trail range –       preload adjuster is sunk inside the cap   Fork Preload –       17mm socket for an anodized adjuster –       initial impression of stiff springs   Fork rebound –       4mm allen [...]",
        "URL": "https://davemosstuning.com/panigale-standard/",
        "Tags": null,
        "Categories": [
            "All",
            "Article",
            "Reviews"
        ]
    },
    {
        "Date": "November 25th, 2017",
        "Title": "Yacugar Shock Fitment",
        "Perex": " YACUGAR SHOCK FITMENT TO A 2009 YAMAHA R1 Why do we want to change out the stock shock for a more expensive replacement? Normally we are looking for added hydraulic features to provide greater adjustability and improve ride quality. In most cases, that isnâ€™t the case with sport bikes as they have all the adjustability [...]",
        "URL": "https://davemosstuning.com/yacugar-shock-fitment/",
        "Tags": null,
        "Categories": [
            "All",
            "Article",
            "How-To"
        ]
    },
    {
        "Date": "April 30th, 2011",
        "Title": "New Premium Template 2019",
        "Perex": " Post Title Here: (ex: 2 Clicks Out: Multistrada 1200 off the showroom floor)    Important: Put video description here. Must be one to two sentences....    ",
        "URL": "https://davemosstuning.com/new-premium-template-2019/",
        "Tags": null,
        "Categories": [
            "All",
            "Video"
        ]
    },
    {
        "Date": "March 5th, 2008",
        "Title": "GSX-R1000 Fork Oil Change 03-08 BETA",
        "Perex": " Dave goes through the process, step by step, of changing fork oil on the 2003 through 2008 Suzuki GSX-R1000.  Freemium Video.  Enjoy.  To view Premium content, please Subscribe or Login.",
        "URL": "https://davemosstuning.com/gsx-r1000-fork-oil-change-03-08-2/",
        "Tags": null,
        "Categories": [
            "All",
            "classroom",
            "Free",
            "How-To",
            "servicing",
            "Suspension",
            "Video"
        ]
    }
]