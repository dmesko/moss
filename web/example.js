var data = [
    {
        "Date": "May 10th, 2019",
        "Title": "FasterClass: Motorcycle Tire Cold \u0026 Hot Tears",
        "Perex": " The common Cold Tear and the Hot Tear are tire pressure issues. There is also a kind of cold tear from bad geometry. Dave explains what to look for and how to fix the problem. Motorcycle tire wear can ruin a tire very quickly, and they're soooooo expensive, why waste them from.",
        "URL": "https://davemosstuning.com/fasterclass-motorcycle-tire-cold-hot-tears/",
        "Tags": [
            "Bridgestone cold tear",
            "cold tear",
            "dunlop cold tear",
            "geometry cold tear",
            "hot tear",
            "michelin cold tear",
            "pirelli cold tear",
            "rear geometry tear"
        ],
        "Categories": [
            "All",
            "classroom",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "May 8th, 2019",
        "Title": "Fixing Bad Motorcycle Handlebar Ergonomics",
        "Perex": " We're talking full one-piece motorcycle handlebars here, not clip-ons. Full handlebars like you see on a BMX bike. They're ALWAYS off center from the factory, and that's just the first problem.",
        "URL": "https://davemosstuning.com/fixing-bad-motorcycle-handlebar-ergonomics/",
        "Tags": [
            "Aprilia",
            "BMW",
            "ergonomics",
            "ergos",
            "handlebar",
            "s1000r",
            "Tuono"
        ],
        "Categories": [
            "All",
            "How-To",
            "servicing",
            "Video"
        ]
    },
    {
        "Date": "May 6th, 2019",
        "Title": "2 Clicks Out: Street Bike Setups \u0026 Ride Day",
        "Perex": " Dave does some suspension setups at a dealership event, then joins the morning ride to do some fine tuning when the suspension heats up.",
        "URL": "https://davemosstuning.com/2-clicks-out-street-bike-setups-ride-day/",
        "Tags": [
            "2 clicks out",
            "675",
            "ergonomics",
            "R6",
            "sag",
            "settings",
            "TL1000",
            "Yamaha"
        ],
        "Categories": [
            "2 clicks out",
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "May 3rd, 2019",
        "Title": "FasterClass: Performance Motorcycle Tires",
        "Perex": " Remember, there are no bad tires in the 21st Century, just misunderstood or mistreated tires. Dave explains how to get actual performance out of performance motorcycle tires.",
        "URL": "https://davemosstuning.com/fasterclass-performance-motorcycle-tires/",
        "Tags": null,
        "Categories": [
            "All",
            "classroom",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "May 2nd, 2019",
        "Title": "Trash or Treasure: GSX-R750 Rebuild Front End",
        "Perex": " With the forks all freshened up we reassemble the front end of our precious 2005 Suzuki GSX-R750.",
        "URL": "https://davemosstuning.com/trash-or-treasure-gsx-r750-rebuild-front-end/",
        "Tags": null,
        "Categories": [
            "All",
            "How-To",
            "servicing",
            "Video"
        ]
    },
    {
        "Date": "April 29th, 2019",
        "Title": "Comparison Test: V4S vs RSV4 1100 Factory",
        "Perex": " Mr Moss test rides and tunes the Ducati V4S and the Aprilia RSV4 1100 Factory back to back. What's good, what's bad, what's fugly, what's fawesome.",
        "URL": "https://davemosstuning.com/comparison-test-v4s-vs-rsv4-1100-factory/",
        "Tags": null,
        "Categories": [
            "All",
            "Reviews",
            "Suspension",
            "Testing Program",
            "Video"
        ]
    },
    {
        "Date": "April 27th, 2019",
        "Title": "FasterClass: Motorcycle Tire Wear Compression",
        "Perex": " Your motorcycle tire will manifest telltale wear patterns indicating whether your compression settings are too hard, too soft, or just right. Dave explains in detail.",
        "URL": "https://davemosstuning.com/fasterclass-motorcycle-tire-wear-compression/",
        "Tags": null,
        "Categories": [
            "All",
            "classroom",
            "Tires",
            "Video"
        ]
    },
    {
        "Date": "April 25th, 2019",
        "Title": "Trash or Treasure: Yamaha YZF 750SP",
        "Perex": " Trash or Treasure: Yamaha YZF 750SP",
        "URL": "https://davemosstuning.com/16038-2/",
        "Tags": null,
        "Categories": [
            "All",
            "Free",
            "Reviews",
            "Video"
        ]
    },
    {
        "Date": "April 22nd, 2019",
        "Title": "2 Clicks Out: Tuono Suspension Setup",
        "Perex": " There aren't too many naked, sit upright superbikes. The Aprilia Tuono is the only one that comes to mind. Luckily it comes with top tier suspension to help manage all that performance.",
        "URL": "https://davemosstuning.com/2-clicks-out-tuono-suspension-setup/",
        "Tags": null,
        "Categories": [
            "2 clicks out",
            "All",
            "Suspension",
            "Video"
        ]
    },
    {
        "Date": "April 19th, 2019",
        "Title": "FasterClass: Motorcycle Tire Age, Discoloration, Cracking",
        "Perex": " We've bundled some odd but common tire problems into one video. Tires can get too old, they can become discolored, and they can crack...",
        "URL": "https://davemosstuning.com/fasterclass-motorcycle-tire-age-discoloration-cracking/",
        "Tags": null,
        "Categories": [
            "All",
            "How-To",
            "Tires",
            "Video"
        ]
    }
]