package dmtfetch

import (
	"reflect"
	"testing"
)

func TestMergeCachedAndNewArticles(t *testing.T) {
	type args struct {
		cached []Article
		recent []Article
	}
	tests := []struct {
		name string
		args args
		want []Article
	}{
		{name: "Do nothing",
			args: args{
				cached: []Article{Article{URL: "1"}, Article{URL: "2"}},
				recent: []Article{}},
			want: []Article{Article{URL: "1"}, Article{URL: "2"}},
		},
		{name: "Simple add",
			args: args{
				cached: []Article{Article{URL: "1"}, Article{URL: "2"}},
				recent: []Article{Article{URL: "0"}}},
			want: []Article{Article{URL: "0"}, Article{URL: "1"}, Article{URL: "2"}},
		},
		{name: "Add just one",
			args: args{
				cached: []Article{Article{URL: "1"}, Article{URL: "2"}},
				recent: []Article{Article{URL: "0"}, Article{URL: "1"}, Article{URL: "2"}}},
			want: []Article{Article{URL: "0"}, Article{URL: "1"}, Article{URL: "2"}},
		},
		{name: "Add two",
			args: args{
				cached: []Article{Article{URL: "1"}, Article{URL: "2"}},
				recent: []Article{Article{URL: "-1"}, Article{URL: "0"}, Article{URL: "1"}, Article{URL: "2"}}},
			want: []Article{Article{URL: "-1"}, Article{URL: "0"}, Article{URL: "1"}, Article{URL: "2"}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MergeCachedAndNewArticles(tt.args.cached, tt.args.recent); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MergeCachedAndNewArticles() = %v, want %v", got, tt.want)
			}
		})
	}
}
