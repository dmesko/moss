package dmtfetch

import (
	"fmt"
	"testing"
)

func TestSlice(t *testing.T) {
	var articles []Article
	fmt.Println(articles)
	articles = append(articles, Article{Title: "blah"})
	fmt.Println(articles)
}
