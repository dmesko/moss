package dmtfetch

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
)

type Article struct {
	Date       string
	Title      string
	Perex      string
	URL        string
	Tags       []string
	Categories []string
}

func ParseWebToArticles(parse func(*goquery.Document) []Article, startPage, maxPages, delay int) ([]Article, error) {
	pageExists := true
	pageCounter := 0
	var articles []Article
	for pageExists && pageCounter < maxPages {
		url := fmt.Sprintf("https://davemosstuning.com/category/all/page/%d/", startPage+pageCounter)
		res, err := http.Get(url)
		if err != nil {
			return nil, err
		}
		defer res.Body.Close()
		if res.StatusCode == 404 {
			pageExists = false
			fmt.Println("\nNot found, final page reached? URL:", url)
			break
		}
		if res.StatusCode != 200 {
			log.Printf("status code error: %d %s", res.StatusCode, res.Status)
			return nil, fmt.Errorf("Status code: %d", res.StatusCode)
		}

		doc, err := goquery.NewDocumentFromReader(res.Body)
		if err != nil {
			return nil, fmt.Errorf("Issue with document parsing: %s", err)
		}
		articles = append(articles, parse(doc)...)
		pageCounter++
		if pageCounter%10 == 0 {
			fmt.Print(pageCounter)
		} else {
			fmt.Print(".")
		}
		time.Sleep(time.Duration(delay) * time.Millisecond)
	}
	return articles, nil
}

func ParseFileToArticles(parse func(*goquery.Document) []Article) []Article {
	var articles []Article
	f, err := os.Open("source.html")
	if err != nil {
		fmt.Println("File can't be opened:", err)
	}
	content := bufio.NewReader(f)
	doc, err := goquery.NewDocumentFromReader(content)
	if err != nil {
		fmt.Println("Issue with document parsing", err)
	}
	articles = append(articles, parse(doc)...)
	return articles
}

func ParsePageToArticles(doc *goquery.Document) []Article {
	perexCleaner, _ := regexp.Compile(`[ ]{3,}.*\[\.\.\.\]`)
	var articles []Article
	doc.Find("div.fusion-post-wrapper").Each(func(i int, s *goquery.Selection) {
		date := s.Find("p.fusion-single-line-meta > span:nth-child(4)").Text()
		title := s.Find("h2 > a").Text()
		perex := s.Find("div.fusion-post-content-container > p").Text()
		perex = perexCleaner.ReplaceAllString(perex, "")

		videoURL := s.Find("h2 > a").AttrOr("href", "n/a")

		var tags []string
		s.Find("span.meta-tags > a").Each(func(i int, s *goquery.Selection) {
			tags = append(tags, s.Text())
		})

		var categories []string
		s.Find("p.fusion-single-line-meta > a[rel='category tag']").Each(func(i int, s *goquery.Selection) {
			categories = append(categories, s.Text())
		})

		a := Article{Date: date, Title: title, Perex: perex, URL: videoURL, Tags: tags, Categories: categories}
		articles = append(articles, a)
	})
	return articles
}

func FormatArticlesToTSV(articles []Article) string {
	var stringBuilder strings.Builder
	for _, article := range articles {
		stringBuilder.WriteString(article.Date + "\t")
		stringBuilder.WriteString(article.Title + "\t")
		stringBuilder.WriteString(article.Perex + "\t")
		stringBuilder.WriteString(article.URL + "\t")
		stringBuilder.WriteString(strings.Join(article.Tags, ",") + "\t")
		stringBuilder.WriteString(strings.Join(article.Categories, ",") + "\t")
		stringBuilder.WriteString("\n")
	}
	return stringBuilder.String()
}

func FormatArticlesToJSON(articles []Article) string {
	json, err := json.MarshalIndent(articles, "", "    ")
	if err != nil {
		fmt.Println("Encoding to JSON failed", err)
	}
	return string(json)
}

func ParseJSONToArticles(data []byte) []Article {
	var articles []Article
	err := json.Unmarshal(data, &articles)
	if err != nil {
		fmt.Println("Encoding to JSON failed", err)
	}
	return articles
}

func MergeCachedAndNewArticles(cached, recent []Article) []Article {
	var newArticles []Article
	for _, recentArticle := range recent {
		inCached := false
		for _, cachedArticle := range cached {
			if recentArticle.URL == cachedArticle.URL {
				inCached = true
				break
			}
		}
		if !inCached {
			newArticles = append(newArticles, recentArticle)
		} else {
			inCached = false
		}
	}
	for _, newOne := range newArticles {
		fmt.Println("New article found: ", newOne.Title)
	}

	return append(newArticles, cached...)
}
