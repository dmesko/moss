package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"time"

	"bitbucket.org/dmesko/moss/dmtfetch"
)

func writeFile(name string, data string) {
	err := ioutil.WriteFile(name, []byte(data), 0644)
	if err != nil {
		fmt.Println("Write to data file failed", err)
	}
}

func main() {
	//getFromFile(parsePageToStruct)
	articles, err := dmtfetch.ParseWebToArticles(dmtfetch.ParsePageToArticles, 1, 100, 1000)
	if err != nil {
		log.Fatalln("\n", err)
	}

	// writeFile("fulldata.tsv", formatCategoriesToTSV(articles))
	writeFile("fulldata.json", dmtfetch.FormatArticlesToJSON(articles))

	updated := fmt.Sprintf("var dateOfUpdate = \"%s\";\n", time.Now().Format("15:04, Jan 2 2006"))
	writeFile("fulldata.js",
		updated+
			"var fullData = "+dmtfetch.FormatArticlesToJSON(articles))
}
