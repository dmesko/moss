// Copyright 2018 Google Inc. All rights reserved.
// Use of this source code is governed by the Apache 2.0
// license that can be found in the LICENSE file.

package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"bitbucket.org/dmesko/moss/dmtfetch"
	"cloud.google.com/go/storage"
	"github.com/NYTimes/gziphandler"
	"google.golang.org/appengine"
	"google.golang.org/appengine/file"
	"google.golang.org/appengine/log"
)

func main() {
	http.HandleFunc("/api/fetchData", fetchData)
	http.HandleFunc("/api/updateData", updateData)

	serveRawJSON := http.HandlerFunc(serveData)
	serveZipJSON := gziphandler.GzipHandler(serveRawJSON)
	http.Handle("/api/serveData", serveZipJSON)

	serveRawTSV := http.HandlerFunc(serveTSVData)
	serveZipTSV := gziphandler.GzipHandler(serveRawTSV)
	http.Handle("/api/serveTSVData", serveZipTSV)

	http.HandleFunc("/api/lastUpdate", lastUpdate)

	appengine.Main()
}

type modified struct {
	Modified string
}

func lastUpdate(w http.ResponseWriter, r *http.Request) {
	d := CreateBucketIO(w, r)
	defer d.client.Close()
	mDate, err := d.lastModified("fulldata.json")
	data := &modified{Modified: ""}
	if err == nil {
		data.Modified = mDate.Format("Jan 02 2006, 15:04")
	}
	w.Header().Set("Content-Type", "application/json")
	json, err := json.Marshal(data)
	w.Write(json)
}

func updateData(w http.ResponseWriter, r *http.Request) {
	if !requestIsAuthorized(w, r) {
		return
	}

	w.Write([]byte("Updating data..."))
	articles, err := dmtfetch.ParseWebToArticles(dmtfetch.ParsePageToArticles, 1, 1, 500)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	data, err := readFileFromBucket(w, r)
	if err != nil {
		return
	}
	cachedArticles := dmtfetch.ParseJSONToArticles(data)

	articles = dmtfetch.MergeCachedAndNewArticles(cachedArticles, articles)

	writeFileToBucket(w, r, articles)
	w.Write([]byte("Done"))
}

func readFileFromBucket(w http.ResponseWriter, r *http.Request) ([]byte, error) {
	d := CreateBucketIO(w, r)
	defer d.client.Close()
	data, err := d.readFile("fulldata.json")
	if err != nil {
		log.Errorf(d.ctx, "Can't read data file: %v", err)
	}
	return data, err
}

func writeFileToBucket(w http.ResponseWriter, r *http.Request, articles []dmtfetch.Article) {
	d := CreateBucketIO(w, r)
	defer d.client.Close()
	fileData := dmtfetch.FormatArticlesToJSON(articles)
	d.createFile("fulldata.json", fileData)

	fileData = dmtfetch.FormatArticlesToTSV(articles)
	d.createFile("fulldata.tsv", fileData)
}

func requestIsAuthorized(w http.ResponseWriter, r *http.Request) bool {
	keys, ok := r.URL.Query()["key"]

	if !ok || len(keys[0]) < 1 {
		w.Write([]byte("Unauthorized"))
		log.Infof(r.Context(), "Url Param 'key' is missing")
		return false
	}
	if keys[0] != "notForAll" {
		w.Write([]byte("Unauthorized"))
		log.Infof(r.Context(), "Key '%s' is wrong => no action", keys[0])
		return false
	}
	return true
}

func fetchData(w http.ResponseWriter, r *http.Request) {
	if !requestIsAuthorized(w, r) {
		return
	}
	w.Write([]byte("Fetching data..."))
	articles, err := dmtfetch.ParseWebToArticles(dmtfetch.ParsePageToArticles, 1, 100, 500)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	writeFileToBucket(w, r, articles)
	w.Write([]byte("Done"))
}

func serveData(w http.ResponseWriter, r *http.Request) {
	d := CreateBucketIO(w, r)
	defer d.client.Close()
	data, err := d.readFile("fulldata.json")
	if err != nil {
		log.Errorf(d.ctx, "Can't read data file: %v", err)
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(data)
}

func serveTSVData(w http.ResponseWriter, r *http.Request) {
	d := CreateBucketIO(w, r)
	defer d.client.Close()
	data, err := d.readFile("fulldata.tsv")
	if err != nil {
		log.Errorf(d.ctx, "Can't read data file: %v", err)
	}
	w.Header().Set("Content-Type", "text/tab-separated-values")
	w.Write(data)
}

func CreateBucketIO(w http.ResponseWriter, r *http.Request) *BucketIO {
	ctx := appengine.NewContext(r)
	// Use `dev_appserver.py --default_gcs_bucket_name because-tires-cant-lie.appspot.com app.yaml`
	// when running locally.
	bucket, err := file.DefaultBucketName(ctx)
	if err != nil {
		log.Errorf(ctx, "failed to get default GCS bucket name: %v", err)
	}

	client, err := storage.NewClient(ctx)
	if err != nil {
		log.Errorf(ctx, "failed to create client: %v", err)
		return nil
	}

	w.Header().Set("Content-Type", "text/plain; charset=utf-8")

	buf := &bytes.Buffer{}
	d := &BucketIO{
		w:          buf,
		ctx:        ctx,
		client:     client,
		bucket:     client.Bucket(bucket),
		bucketName: bucket,
	}
	return d
}

type BucketIO struct {
	client     *storage.Client
	bucketName string
	bucket     *storage.BucketHandle
	w          io.Writer
	ctx        context.Context
	failed     bool
}

func (d *BucketIO) errorf(format string, args ...interface{}) {
	d.failed = true
	fmt.Fprintln(d.w, fmt.Sprintf(format, args...))
	log.Errorf(d.ctx, format, args...)
}

func (d *BucketIO) createFile(fileName, content string) {
	fmt.Fprintf(d.w, "Creating file /%v/%v\n", d.bucketName, fileName)

	wc := d.bucket.Object(fileName).NewWriter(d.ctx)

	wc.ContentType = "application/json"

	if _, err := wc.Write([]byte(content)); err != nil {
		d.errorf("createFile: unable to write data to bucket %q, file %q: %v", d.bucketName, fileName, err)
		return
	}
	if err := wc.Close(); err != nil {
		d.errorf("createFile: unable to close bucket %q, file %q: %v", d.bucketName, fileName, err)
		return
	}
}

func (d *BucketIO) lastModified(fileName string) (*time.Time, error) {
	o := d.bucket.Object(fileName)
	attrs, err := o.Attrs(d.ctx)
	if err != nil {
		return nil, err
	}
	return &attrs.Updated, nil
}

func (d *BucketIO) readFile(fileName string) ([]byte, error) {
	rc, err := d.bucket.Object(fileName).NewReader(d.ctx)
	if err != nil {
		d.errorf("readFile: unable to open file from bucket %q, file %q: %v", d.bucketName, fileName, err)
		return []byte{}, err
	}
	defer rc.Close()
	slurp, err := ioutil.ReadAll(rc)
	if err != nil {
		d.errorf("readFile: unable to read data from bucket %q, file %q: %v", d.bucketName, fileName, err)
		return []byte{}, err
	}
	return slurp, nil
}
